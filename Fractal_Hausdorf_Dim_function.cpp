//#include "Removing_Labels_Fr_Color_Images_2.h"
#include "Fractal_Hausdorf_Dim_2.h"

using namespace imago;

FILE *fout_lr;

int doFractal_DimensionOf_ColorImage(
	const Image& image_in,

	//const PARAMETERS_REMOVAL_OF_LABELS *sParameters_Removal_Of_Labelsf,
	float fOneDim_Lacunar_Arrf[], 

	float &fFractal_Dimension)
{
	int SetOfLogPoints_ForFractal_Dim(
		const int nThresholdForIntensitiesMinf,
		const int nThresholdForIntensitiesMaxf,
		const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		const COLOR_IMAGE *sColor_ImageInitf,

		int &nNumOfLogPointsf,
		float fNegLogOfLenOfSquare_Arrf[], //[nNumOfIters_ForDim_2powerN_Max]

		float fLogPoints_Arrf[]); // [nNumOfIters_ForDim_2powerN_Max]

	void SlopeOfAStraightLine(
		const int nDimf,

		const float fX_Arrf[],
		const float fY_Arrf[],

		float &fSlopef);

	int Initializing_Color_To_CurSize(
		const int nImageWidthf,
		const int nImageLengthf,

		COLOR_IMAGE *sColor_Imagef);
	   
	////////////////////////////////////////////////////////////////////////
//Tug-of-war lacunarity�A novel approach for estimating lacunarity, https://aip.scitation.org/doi/full/10.1063/1.4966539
	int LacunarityOfMasses_At_All_Intensity_Ranges(

		const int nNumOfSquareOccurrence_Intervalsf,// == 4 <= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

		//const int nDim_2pNf,

		const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,
		const int nLenOfSquareMinf,

		const COLOR_IMAGE *sColor_ImageInitf,

	///////////////////////////////////////
		float fOneDim_Lacunar_Arrf[]); //[nNumOfLacunarFeasFor_OneDimTot] //nNumOfLacunarFeasFor_OneDimTot = nNumOfFeasForLacunar_AtIntensity_Range*nNumOfIntensity_IntervalsForLacunarTot

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	int
		nRes,
		i,
		j,

		iFeaf,
		nSizeOfImage, // = nImageWidth*nImageHeight,

		nIndexOfPixelCur,

		iLen,
		iWid,
		nNumOfLogPointsf,

		nRed,
		nGreen,
		nBlue,

		nIntensity_Read_Test_ImageMax = -nLarge,
		nNumOfLensOfSquareTot_Actualf, //
		nImageWidth,
		nImageHeight;

	float
		fSlope,//
		fNegLogOfLenOfSquare_Arr[nNumOfIters_ForDim_2powerN_Max], //[]

		fLogPoints_Arr[nNumOfIters_ForDim_2powerN_Max];

	////////////////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
	fout_lr = fopen("wMain_Hausdorf_Dim.txt", "w");

	if (fout_lr == NULL)
	{
		printf("\n\n fout_lr == NULL");
		//getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (fout_lr == NULL)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	  // size of image
	nImageWidth = image_in.width();
	nImageHeight = image_in.height();

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n nImageWidth = %d, nImageHeight = %d", nImageWidth, nImageHeight);
	fprintf(fout_lr, "\n\n nImageWidth = %d, nImageHeight = %d", nImageWidth, nImageHeight);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (nImageWidth > nLenMax || nImageWidth < nLenMin)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in reading the image width: nImageWidth = %d", nImageWidth);
		printf("\n\n An error in reading the image width: nImageWidth = %d", nImageWidth);
		getchar(); exit(1);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} // if (nImageWidth > nLenMax || nImageWidth < nLenMin)

	if (nImageHeight > nWidMax || nImageHeight < nWidMin)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in reading the image height: nImageHeight = %d", nImageHeight);
		fprintf(fout_lr, "\n\n An error in reading the image height: nImageHeight = %d", nImageHeight);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} // if (nImageHeight > nWidMax || nImageHeight < nWidMin)

	int bytesOfWidth = image_in.pitchInBytes();

	Image imageToSave(nImageWidth, nImageHeight, bytesOfWidth);

	///////////////////////////////////////////////////////
	COLOR_IMAGE sColor_Image; //

	nRes = Initializing_Color_To_CurSize(
		nImageHeight, //const int nImageWidthf,
		nImageWidth, //const int nImageLengthf,

		&sColor_Image); // COLOR_IMAGE *sColor_Imagef);

	if (nRes == UNSUCCESSFUL_RETURN)
	{
		//if (sColor_Imagef->nLenObjectBoundary_Arr == nullptr || sColor_Imagef->nRed_Arr == nullptr || sColor_Imagef->nGreen_Arr == nullptr || sColor_Imagef->nBlue_Arr == nullptr ||
			//sColor_Imagef->nIsAPixelBackground_Arr == nullptr)

		delete[] sColor_Image.nLenObjectBoundary_Arr;

		delete[] sColor_Image.nRed_Arr;
		delete[] sColor_Image.nGreen_Arr;
		delete[] sColor_Image.nBlue_Arr;
		delete[] sColor_Image.nIsAPixelBackground_Arr;

		return UNSUCCESSFUL_RETURN;
	} //if (nRes == UNSUCCESSFUL_RETURN)

	nSizeOfImage = nImageWidth * nImageHeight;

	//finding 'nIntensity_Read_Test_ImageMax' to decide if the intensity rescaling to (0,255) is needed
	for (j = 0; j < nImageHeight; j++)
	{
		for (i = 0; i < nImageWidth; i++)
		{
			nIndexOfPixelCur = i + j * nImageWidth;

			nRed = image_in(j, i, R);
			nGreen = image_in(j, i, G);
			nBlue = image_in(j, i, B);

			if (nRed < 0 || nRed > nIntensityStatForReadMax || nGreen < 0 || nGreen > nIntensityStatForReadMax || nBlue < 0 || nBlue > nIntensityStatForReadMax)
			{
#ifndef COMMENT_OUT_ALL_PRINTS

				fprintf(fout_lr, "\n\n An error in reading the image: nRed = %d, nGreen = %d, nBlue = %d, i = %d, j = %d", nRed, nGreen, nBlue, i, j);
				printf("\n\n An error in reading the image: nRed = %d, nGreen = %d, nBlue = %d, i = %d, j = %d", nRed, nGreen, nBlue, i, j);
				//printf("\n\n Please press any key to exit");
				//getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			} // if (nRed < 0 || nRed > nIntensityStatForReadMax || nGreen < 0 || nGreen > nIntensityStatForReadMax || nBlue < 0 || nBlue > nIntensityStatForReadMax)

			if (nRed > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nRed;

			if (nGreen > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nGreen;

			if (nBlue > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nBlue;

		} // for (int i = 0; i < nImageWidth; i++)

	}//for (int j = 0; j < nImageHeight; j++)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
	fprintf(fout_lr, "\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	////////////////////////////////////////////////////////////////////////////////////////////

	for (j = 0; j < nImageHeight; j++)
	{
		for (i = 0; i < nImageWidth; i++)
		{
			nIndexOfPixelCur = i + j * nImageWidth;

			nRed = image_in(j, i, R);
			nGreen = image_in(j, i, G);
			nBlue = image_in(j, i, B);

			iLen = i;
			iWid = j;

			if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
			{
				//rescaling
				sColor_Image.nRed_Arr[nIndexOfPixelCur] = nRed / 256;

				sColor_Image.nGreen_Arr[nIndexOfPixelCur] = nGreen / 256;
				sColor_Image.nBlue_Arr[nIndexOfPixelCur] = nBlue / 256;

				//////////////////////////////////////////////////////////////////////////////////////////////////////
				sColor_Image.nRed_Arr[nIndexOfPixelCur] = nRed;
				sColor_Image.nGreen_Arr[nIndexOfPixelCur] = nGreen;
				sColor_Image.nBlue_Arr[nIndexOfPixelCur] = nBlue;

			} //if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
			else
			{
				// no rescaling
				sColor_Image.nRed_Arr[nIndexOfPixelCur] = nRed;
				sColor_Image.nGreen_Arr[nIndexOfPixelCur] = nGreen;
				sColor_Image.nBlue_Arr[nIndexOfPixelCur] = nBlue;
			} //else

		} // for (int i = 0; i < nImageWidth; i++)
	}//for (int j = 0; j < nImageHeight; j++)
///////////////////////////////////////////////////////////////////////////
	WEIGHTES_OF_RGB_COLORS sWeightsOfColorsf;

	sWeightsOfColorsf.fWeightOfRed = fWeightOfRed_InStr;
	sWeightsOfColorsf.fWeightOfGreen = fWeightOfGreen_InStr;
	sWeightsOfColorsf.fWeightOfBlue = fWeightOfBlue_InStr;

	//printf("\n 4"); getchar();

	nRes = SetOfLogPoints_ForFractal_Dim(
		nThresholdForIntensities_ForFractalDimMin, //const int nThresholdForIntensitiesMinf,
		nThresholdForIntensities_ForFractalDimMax, //const int nThresholdForIntensitiesMaxf,

		&sWeightsOfColorsf, //const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

				&sColor_Image, //const COLOR_IMAGE *sColor_ImageInitf,

				nNumOfLogPointsf, //int &nNumOfLogPointsf,
				fNegLogOfLenOfSquare_Arr, //int nLenOfSquare_Arrf[], //[nNumOfIters_ForDim_2powerN_Max]

				fLogPoints_Arr); // float fLogPoints_Arrf[]); // [nNumOfIters_ForDim_2powerN_Max]
	
	if (nRes == UNSUCCESSFUL_RETURN)
	{
		delete[] sColor_Image.nRed_Arr;
		delete[] sColor_Image.nGreen_Arr;
		delete[] sColor_Image.nBlue_Arr;

		delete[] sColor_Image.nLenObjectBoundary_Arr;

		delete[] sColor_Image.nIsAPixelBackground_Arr;
		return UNSUCCESSFUL_RETURN;
	}// if (nRes == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n  After 'SetOfLogPoints_ForFractal_Dim': nNumOfLogPointsf = %d", nNumOfLogPointsf);
	fprintf(fout_lr, "\n\n  After 'SetOfLogPoints_ForFractal_Dim': nNumOfLogPointsf = %d", nNumOfLogPointsf);
	//getchar();

	fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	///////////////////////////////////////////////////////
	for (i = 0; i < nNumOfLogPointsf; i++)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 2: i = %d, fNegLogOfLenOfSquare_Arr[i] = %E, fLogPoints_Arr[i] = %E", i, fNegLogOfLenOfSquare_Arr[i], fLogPoints_Arr[i]);
		fprintf(fout_lr, "\n\n 'SetOfLogPoints_ForFractal_Dim'  2: i = %d, fNegLogOfLenOfSquare_Arr[i] = %E, fLogPoints_Arr[i] = %E", i, fNegLogOfLenOfSquare_Arr[i], fLogPoints_Arr[i]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	}//for (i = 0; i < nDimf; i++)

	SlopeOfAStraightLine(
		nNumOfLogPointsf, //const int nDimf,

		fNegLogOfLenOfSquare_Arr, //const float fX_Arrf[],
		fLogPoints_Arr, //const float fY_Arrf[],

		fSlope); // float &fSlopef);

	fFractal_Dimension = fSlope;
/////////////////////////////////////////////////////////////////////

	nRes = LacunarityOfMasses_At_All_Intensity_Ranges(
		nNumOfSquareOccurrence_Intervals, //const int nNumOfSquareOccurrence_Intervalsf,// == 4 <= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

		//const int nDim_2pNf,

		&sWeightsOfColorsf, //const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,
		nLenOfSquareMin, //const int nLenOfSquareMinf,

		&sColor_Image, //const COLOR_IMAGE *sColor_ImageInitf,

		///////////////////////////////////////
		fOneDim_Lacunar_Arrf); // float fOneDim_Lacunar_Arrf[]); //[nNumOfLacunarFeasFor_OneDimTot] //nNumOfLacunarFeasFor_OneDimTot = nNumOfFeasForLacunar_AtIntensity_Range*nNumOfIntensity_IntervalsForLacunarTot

	if (nRes == UNSUCCESSFUL_RETURN)
	{
		delete[] sColor_Image.nRed_Arr;
		delete[] sColor_Image.nGreen_Arr;
		delete[] sColor_Image.nBlue_Arr;

		delete[] sColor_Image.nLenObjectBoundary_Arr;

		delete[] sColor_Image.nIsAPixelBackground_Arr;
		return UNSUCCESSFUL_RETURN;
	}// if (nRes == UNSUCCESSFUL_RETURN)

		
//////////////////////////////////////////////////////////
	delete[] sColor_Image.nRed_Arr;
	delete[] sColor_Image.nGreen_Arr;
	delete[] sColor_Image.nBlue_Arr;

	delete[] sColor_Image.nLenObjectBoundary_Arr;
	delete[] sColor_Image.nIsAPixelBackground_Arr;

#ifndef COMMENT_OUT_ALL_PRINTS
	fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	/////////////////////////////////////////////////
	return SUCCESSFUL_RETURN;
} //int doFractal_DimensionOf_ColorImage(...
////////////////////////////////////////////////////////////////////////////////////////////////

int Initializing_Color_To_CurSize(
	const int nImageWidthf,
	const int nImageLengthf,

	COLOR_IMAGE *sColor_Imagef) //[]
{
	int
		nIndexOfPixelCurf,

		nImageSizeCurf = nImageWidthf * nImageLengthf,
		iWidf,
		iLenf;

	sColor_Imagef->nSideOfObjectLocation = 0; // neither left nor right
	sColor_Imagef->nIntensityOfBackground_Red = -1;
	sColor_Imagef->nIntensityOfBackground_Green = -1;
	sColor_Imagef->nIntensityOfBackground_Blue = -1;

	sColor_Imagef->nWidth = nImageWidthf;
	sColor_Imagef->nLength = nImageLengthf;

	//sGrayscale_Imagef->nPixel_ValidOrNotArr = new int[nImageSizeCurf];

	sColor_Imagef->nLenObjectBoundary_Arr = new int[nImageWidthf];

	sColor_Imagef->nRed_Arr = new int[nImageSizeCurf];
	sColor_Imagef->nGreen_Arr = new int[nImageSizeCurf];
	sColor_Imagef->nBlue_Arr = new int[nImageSizeCurf];

	sColor_Imagef->nIsAPixelBackground_Arr = new int[nImageSizeCurf];

	if (sColor_Imagef->nLenObjectBoundary_Arr == nullptr || sColor_Imagef->nRed_Arr == nullptr || sColor_Imagef->nGreen_Arr == nullptr || sColor_Imagef->nBlue_Arr == nullptr ||
		sColor_Imagef->nIsAPixelBackground_Arr == nullptr)
	{
		return UNSUCCESSFUL_RETURN;
	} //if (sColor_Imagef->nLenObjectBoundary_Arr == nullptr || sColor_Imagef->nRed_Arr == nullptr || ...

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = -1;

		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);

			//sGrayscale_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] = 1; //valid for no restrictions
			sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = -1;
			sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = -1;
			sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = -1;

			sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 0; //all pixels belong to the object
		} //for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
	} // for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	return SUCCESSFUL_RETURN;
} //int Initializing_Color_To_CurSize(...
/////////////////////////////////////////////////////////////////////////////

int Initializing_Embedded_Image(
	const int nDim_2pNf,

	EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef) // //[nDim_2pNf*nDim_2pNf]
{
	int
		nIndexOfPixelInEmbeddedImagef,
		nImageSizeCurf = nDim_2pNf * nDim_2pNf,

		iWidf,
		iLenf;

	sImageEmbeddedf_ForLacunarityBlackWhitef->nWidth = nDim_2pNf;
	sImageEmbeddedf_ForLacunarityBlackWhitef->nLength = nDim_2pNf;

	sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr = new int[nImageSizeCurf];

	if (sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr == nullptr)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Initializing_Embedded_Image': sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr == nullptr");
		fprintf(fout_lr, "\n\n An error in 'Initializing_Embedded_Image': sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr == nullptr");
		 getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		return UNSUCCESSFUL_RETURN;
	} //if (sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr == nullptr)

	for (iWidf = 0; iWidf < nDim_2pNf; iWidf++)
	{

		for (iLenf = 0; iLenf < nDim_2pNf; iLenf++)
		{
			nIndexOfPixelInEmbeddedImagef = iLenf + (iWidf*nDim_2pNf);

			sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] = -1; //invalid

		} //for (iLenf = 0; iLenf < nDim_2pNf; iLenf++)
	} // for (iWidf = 0; iWidf < nDim_2pNf; iWidf++)

	return SUCCESSFUL_RETURN;
} //int Initializing_Embedded_Image(...
  ///////////////////////////////////////////////////////////////////////

int Dim_2powerN(
	const int nLengthf,
	const int nWidthf,

	int &nScalef,
	int &nDim_2pNf)
{
	int
		i,
		nTempf,
		nLargerDimInitf;

	if (nLengthf >= nWidthf)
		nLargerDimInitf = nLengthf;
	else
		nLargerDimInitf = nWidthf;

	nTempf = 2;
	for (i = 2; i < nNumOfIters_ForDim_2powerN_Max; i++)
	{
		nTempf = nTempf * 2;
		if (nTempf >= nLargerDimInitf)
		{
			nDim_2pNf = nTempf;
			break;
		}//if (nTempf >= nLargerDimInitf)
	}//for (i = 2; i < nNumOfIters_ForDim_2powerN_Max; i++)
/////////////////////////////////

	if (i == 3 || i >= nNumOfIters_ForDim_2powerN_Max - 1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf( "\n\n An error in 'Dim_2powerN': i = %d, nNumOfIters_ForDim_2powerN_Max - 1 = %dE", i, nNumOfIters_ForDim_2powerN_Max - 1);
		fprintf(fout_lr, "\n\n An error in 'Dim_2powerN': i = %d, nNumOfIters_ForDim_2powerN_Max - 1 = %dE", i, nNumOfIters_ForDim_2powerN_Max - 1);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		return UNSUCCESSFUL_RETURN;
	}
	else
	{
		nScalef = i;
		return SUCCESSFUL_RETURN;
	} //
}//int Dim_2powerN(...
/////////////////////////////////////////////////////////////////////////////

int Embedding_Image_Into_2powerN_ForHausdorff(
	const int nDim_2pNf,

	const int nThresholdForIntensitiesMinf,
	const int nThresholdForIntensitiesMaxf,

	const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

	const COLOR_IMAGE *sColor_ImageInitf,
	EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef) //[nDim_2pNf*nDim_2pNf]

{
	int
		nIndexOfPixelCurf,
		nIndexOfPixelInEmbeddedImagef,

		iWidf,
		iLenf,

		nNumOfWhitePixelsTotf = 0,
		nNumOfBlackPixelsTotf = 0,

		nRedInitf,
		nGreenInitf,
		nBlueInitf,

		nDivisorf = 3,

		nRedCurf,
		nGreenCurf,
		nBlueCurf,
		nIntensityAverf;

	float
		fRatiof;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'Embedding_Image_Into_2powerN_ForHausdorff'");
	fprintf(fout_lr, "\n nDim_2pNf = %d, nThresholdForIntensitiesMinf = %d, nThresholdForIntensitiesMaxf = %d", nDim_2pNf, nThresholdForIntensitiesMinf, nThresholdForIntensitiesMaxf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	{
		nDivisorf = 3;
	} // if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
	{
		nDivisorf = 2;
	} //else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)

	else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	{
		nDivisorf = 2;
	} //else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	{
		nDivisorf = 2;
	} //else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)

	else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
	{
		nDivisorf = 1;
	} //else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
	else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
	{
		nDivisorf = 1;
	} //else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
	else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	{
		nDivisorf = 1;
	} //else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	else
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Embedding_Image_Into_2powerN_ForHausdorff': the color weights are wrong");
		printf( "\n\n sWeightsOfColorsf->fWeightOfRed = %E, sWeightsOfColorsf->fWeightOfGreen = %E, sWeightsOfColorsf->fWeightOfBlue = %E",
			sWeightsOfColorsf->fWeightOfRed, sWeightsOfColorsf->fWeightOfGreen, sWeightsOfColorsf->fWeightOfBlue);

		printf( "\n nDim_2pNf = %d, nThresholdForIntensitiesMinf = %d, nThresholdForIntensitiesMaxf = %d", nDim_2pNf, nThresholdForIntensitiesMinf, nThresholdForIntensitiesMaxf);

		fprintf(fout_lr, "\n\n An error in 'Embedding_Image_Into_2powerN_ForHausdorff': the color weights are wrong");

		fprintf(fout_lr, "\n\n sWeightsOfColorsf->fWeightOfRed = %E, sWeightsOfColorsf->fWeightOfGreen = %E, sWeightsOfColorsf->fWeightOfBlue = %E",
			sWeightsOfColorsf->fWeightOfRed, sWeightsOfColorsf->fWeightOfGreen, sWeightsOfColorsf->fWeightOfBlue);

		fprintf(fout_lr, "\n nDim_2pNf = %d, nThresholdForIntensitiesMinf = %d, nThresholdForIntensitiesMaxf = %d", nDim_2pNf, nThresholdForIntensitiesMinf, nThresholdForIntensitiesMaxf);

		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		return UNSUCCESSFUL_RETURN;
	}//else

	for (iWidf = 0; iWidf < nDim_2pNf; iWidf++)
	{
		for (iLenf = 0; iLenf < nDim_2pNf; iLenf++)
		{
			nIndexOfPixelInEmbeddedImagef = iLenf + (iWidf*nDim_2pNf);

			if (iWidf < sColor_ImageInitf->nWidth && iLenf < sColor_ImageInitf->nLength)
			{
				nIndexOfPixelCurf = iLenf + iWidf * sColor_ImageInitf->nLength;

				nRedInitf = sColor_ImageInitf->nRed_Arr[nIndexOfPixelCurf];
				nGreenInitf = sColor_ImageInitf->nGreen_Arr[nIndexOfPixelCurf];
				nBlueInitf = sColor_ImageInitf->nBlue_Arr[nIndexOfPixelCurf];

				nRedCurf = (int)((float)(nRedInitf)* sWeightsOfColorsf->fWeightOfRed);
				nBlueCurf = (int)((float)(nGreenInitf)* sWeightsOfColorsf->fWeightOfGreen);
				nGreenCurf = (int)((float)(nBlueInitf)* sWeightsOfColorsf->fWeightOfBlue);

				nIntensityAverf = (nRedCurf + nBlueCurf + nGreenCurf) / nDivisorf;

				if (nIntensityAverf > nIntensityStatMax)
					nIntensityAverf = nIntensityStatMax;

				if (nIntensityAverf >= nThresholdForIntensitiesMinf && nIntensityAverf <= nThresholdForIntensitiesMaxf)
				{
					sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] = 1; //
					nNumOfWhitePixelsTotf += 1;
				} //if (nIntensityAverf >= nThresholdForIntensitiesMinf && nIntensityAverf <= nThresholdForIntensitiesMaxf)
				else
				{
					sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] = 0;
					nNumOfBlackPixelsTotf += 1;
				}//else if (nIntensityAverf <= nThresholdForIntensitiesf)

			} // if (iWidf < sColor_ImageInitf->nWidth && iLenf < sColor_ImageInitf->nLength)
			else
			{
				sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] = 0; // no nonzero pixels
				nNumOfBlackPixelsTotf += 1;
			}//else

		} //for (iLenf = 0; iLenf < nDim_2pNf; iLenf++)
	} // for (iWidf = 0; iWidf < nDim_2pNf; iWidf++)

	fRatiof = (float)(nNumOfWhitePixelsTotf) / (float)(nDim_2pNf*nDim_2pNf);

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n The end of 'Embedding_Image_Into_2powerN_ForHausdorff': nNumOfWhitePixelsTotf = %d, nNumOfBlackPixelsTotf = %d, fRatiof = %E", nNumOfWhitePixelsTotf, nNumOfBlackPixelsTotf, fRatiof);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	return SUCCESSFUL_RETURN;
} //int Embedding_Image_Into_2powerN_ForHausdorff(...

//////////////////////////////////////////////////////////
int Embedding_Image_Into_2powerN_ForLacunarity(
	const int nDim_2pNf,

	const int nThresholdForIntensitiesMinf, //>=
	const int nThresholdForIntensitiesMaxf, // <

	const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

	const COLOR_IMAGE *sColor_ImageInitf,
	EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef) //[nDim_2pNf*nDim_2pNf]

{
	int
		nIndexOfPixelCurf,
		nIndexOfPixelInEmbeddedImagef,

		iWidf,
		iLenf,

		nNumOfWhitePixelsTotf = 0,
		nNumOfBlackPixelsTotf = 0,

		nRedInitf,
		nGreenInitf,
		nBlueInitf,

		nDivisorf = 3,

		nRedCurf,
		nGreenCurf,
		nBlueCurf,
		nIntensityAverf;

	float
		fRatiof;

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'Embedding_Image_Into_2powerN_ForLacunarity': sColor_ImageInitf->nLength = %d, nDim_2pNf = %d, sColor_ImageInitf->nWidth = %d",
		sColor_ImageInitf->nLength, nDim_2pNf, sColor_ImageInitf->nWidth); //

	fprintf(fout_lr, "\n\n 'Embedding_Image_Into_2powerN_ForLacunarity': sColor_ImageInitf->nLength = %d, nDim_2pNf = %d, sColor_ImageInitf->nWidth = %d",
		sColor_ImageInitf->nLength, nDim_2pNf, sColor_ImageInitf->nWidth); //
	
	fflush(fout_lr);// getchar();
	
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	{
		nDivisorf = 3;
	} // if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
	{
		nDivisorf = 2;
	} //else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)

	else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	{
		nDivisorf = 2;
	} //else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	{
		nDivisorf = 2;
	} //else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)

	else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
	{
		nDivisorf = 1;
	} //else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
	else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
	{
		nDivisorf = 1;
	} //else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
	else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	{
		nDivisorf = 1;
	} //else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	else
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Embedding_Image_Into_2powerN_ForLacunarity': the color weights are wrong");

		fprintf(fout_lr, "\n\n An error in 'Embedding_Image_Into_2powerN_ForLacunarity': the color weights are wrong");
		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		return UNSUCCESSFUL_RETURN;
	}//else


	//printf("\n 'Embedding_Image_Into_2powerN_ForLacunarity' 1"); getchar();

	if (sColor_ImageInitf->nLength > nDim_2pNf || sColor_ImageInitf->nWidth > nDim_2pNf)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Embedding_Image_Into_2powerN_ForLacunarity': sColor_ImageInitf->nLength = %d > nDim_2pNf = %d || sColor_ImageInitf->nWidth = %d > nDim_2pNf",
			sColor_ImageInitf->nLength,nDim_2pNf,sColor_ImageInitf->nWidth);

		fprintf(fout_lr, "\n\n An error in 'Embedding_Image_Into_2powerN_ForLacunarity': sColor_ImageInitf->nLength = %d > nDim_2pNf = %d || sColor_ImageInitf->nWidth = %d > nDim_2pNf",
			sColor_ImageInitf->nLength, nDim_2pNf, sColor_ImageInitf->nWidth);

		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		return UNSUCCESSFUL_RETURN;

	}//if (sColor_ImageInitf->nLength > nDim_2pNf || sColor_ImageInitf->nWidth > nDim_2pNf)
		
	for (iWidf = 0; iWidf < nDim_2pNf; iWidf++)
	{
#ifndef COMMENT_OUT_ALL_PRINTS	
		printf("\n 'Embedding_Image_Into_2powerN_ForLacunarity': iWidf = %d", iWidf);
		fprintf(fout_lr,"\n 'Embedding_Image_Into_2powerN_ForLacunarity': iWidf = %d", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		for (iLenf = 0; iLenf < nDim_2pNf; iLenf++)
		{
			nIndexOfPixelInEmbeddedImagef = iLenf + (iWidf*nDim_2pNf);

			if (iWidf < sColor_ImageInitf->nWidth && iLenf < sColor_ImageInitf->nLength)
			{
				nIndexOfPixelCurf = iLenf + iWidf * sColor_ImageInitf->nLength;

				nRedInitf = sColor_ImageInitf->nRed_Arr[nIndexOfPixelCurf];
				nGreenInitf = sColor_ImageInitf->nGreen_Arr[nIndexOfPixelCurf];
				nBlueInitf = sColor_ImageInitf->nBlue_Arr[nIndexOfPixelCurf];

				nRedCurf = (int)((float)(nRedInitf)* sWeightsOfColorsf->fWeightOfRed);
				nBlueCurf = (int)((float)(nGreenInitf)* sWeightsOfColorsf->fWeightOfGreen);
				nGreenCurf = (int)((float)(nBlueInitf)* sWeightsOfColorsf->fWeightOfBlue);

				nIntensityAverf = (nRedCurf + nBlueCurf + nGreenCurf) / nDivisorf;

				if (nIntensityAverf > nIntensityStatMax)
					nIntensityAverf = nIntensityStatMax;

				if (nIntensityAverf >= nThresholdForIntensitiesMinf && nIntensityAverf < nThresholdForIntensitiesMaxf)
				{
					sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] = 1; //
					nNumOfWhitePixelsTotf += 1;
				} //if (nIntensityAverf >= nThresholdForIntensitiesMinf && nIntensityAverf < nThresholdForIntensitiesMaxf)
				else
				{
					sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] = 0;
					nNumOfBlackPixelsTotf += 1;
				}//else if (nIntensityAverf <= nThresholdForIntensitiesf)

			} // if (iWidf < sColor_ImageInitf->nWidth && iLenf < sColor_ImageInitf->nLength)
			else
			{
				sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] = 0; // no nonzero pixels
				nNumOfBlackPixelsTotf += 1;
			}//else

		///	fprintf(fout_lr, "\n iWidf = %d, iLenf = %d, sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr[%d] = %d",
			//	iWidf, iLenf, nIndexOfPixelInEmbeddedImagef,sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef]);
			//fflush(fout_lr);

		} //for (iLenf = 0; iLenf < nDim_2pNf; iLenf++)

	} // for (iWidf = 0; iWidf < nDim_2pNf; iWidf++)

	//printf("\n 'Embedding_Image_Into_2powerN_ForLacunarity' 2"); getchar();

	fRatiof = (float)(nNumOfWhitePixelsTotf) / (float)(nDim_2pNf*nDim_2pNf);

#ifndef COMMENT_OUT_ALL_PRINTS
	printf( "\n\n The end of 'Embedding_Image_Into_2powerN_ForLacunarity': nNumOfWhitePixelsTotf = %d, nNumOfBlackPixelsTotf = %d, fRatiof = %E", nNumOfWhitePixelsTotf, nNumOfBlackPixelsTotf, fRatiof);
	fprintf(fout_lr, "\n\n The end of 'Embedding_Image_Into_2powerN_ForLacunarity': nNumOfWhitePixelsTotf = %d, nNumOfBlackPixelsTotf = %d, fRatiof = %E", nNumOfWhitePixelsTotf, nNumOfBlackPixelsTotf, fRatiof);
	//printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	return SUCCESSFUL_RETURN;
} //int Embedding_Image_Into_2powerN_ForLacunarity(...
///////////////////////////////////////////////////////////////////////////////

int NumOfObjectSquaresAndLogPoint_WithResolution(
	const int nDim_2pNf,

	const int nLenOfSquaref,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	int &nNumOfObjectSquaresTotf,
	float &fLogPointf)

{
	int
		nIndexOfPixelInEmbeddedImagef,

		nIndexOfPixelMaxf = nDim_2pNf* nDim_2pNf,

		iWidf,
		iLenf,

		nNumOfSquaresInImageSidef = nDim_2pNf / nLenOfSquaref,

		nNumOfSquaresInImageTotf, // = nNumOfSquaresInImageSidef* nNumOfSquaresInImageSidef,

		nWidOfSquareMinf,
		nWidOfSquareMaxf,

		nLenOfSquareMinf,
		nLenOfSquareMaxf,

		iWidSquaresf,
		iLenSquaresf;

	float
		fRatioOfSquaresf;
	////////////////////////////////////////////////
	nNumOfSquaresInImageTotf = nNumOfSquaresInImageSidef * nNumOfSquaresInImageSidef;

	nNumOfObjectSquaresTotf = 0;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'NumOfObjectSquaresAndLogPoint_WithResolution'\n");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iWidSquaresf = 0; iWidSquaresf < nNumOfSquaresInImageSidef; iWidSquaresf++)
	{
		nWidOfSquareMinf = iWidSquaresf * nLenOfSquaref;

		if (nWidOfSquareMinf > sColor_ImageInitf->nWidth)
		{
			continue;
		}//if (nWidOfSquareMinf > sColor_ImageInitf->nWidth)

		nWidOfSquareMaxf = (iWidSquaresf + 1)* nLenOfSquaref;

		for (iLenSquaresf = 0; iLenSquaresf < nNumOfSquaresInImageSidef; iLenSquaresf++)
		{
			nLenOfSquareMinf = iLenSquaresf * nLenOfSquaref;
			nLenOfSquareMaxf = (iLenSquaresf + 1)* nLenOfSquaref;

			if (nLenOfSquareMinf > sColor_ImageInitf->nLength)
			{
				continue;
			}//if (nLenOfSquareMinf > sColor_ImageInitf->nLength)

			for (iWidf = nWidOfSquareMinf; iWidf < nWidOfSquareMaxf; iWidf++)
			{
				for (iLenf = nWidOfSquareMinf; iLenf < nLenOfSquareMaxf; iLenf++)
				{
					nIndexOfPixelInEmbeddedImagef = iLenf + (iWidf*nDim_2pNf);

					if (nIndexOfPixelInEmbeddedImagef >= nIndexOfPixelMaxf)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						printf("\n\n An error in 'NumOfObjectSquaresAndLogPoint_WithResolution': nIndexOfPixelInEmbeddedImagef = %d >= nIndexOfPixelMaxf = %d", nIndexOfPixelInEmbeddedImagef,nIndexOfPixelMaxf);
						fprintf(fout_lr, "\n\n An error in 'NumOfObjectSquaresAndLogPoint_WithResolution': nIndexOfPixelInEmbeddedImagef = %d >= nIndexOfPixelMaxf = %d", nIndexOfPixelInEmbeddedImagef, nIndexOfPixelMaxf);


#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

						return UNSUCCESSFUL_RETURN;
					}//if (nIndexOfPixelInEmbeddedImagef >= nIndexOfPixelMaxf)

					if (sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] == 1)
					{
						nNumOfObjectSquaresTotf += 1;

						goto MarkContinueForiLenSquares;
					} //if (sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] == 1)

				} //for (iLenf = nWidOfSquareMinf; iLenf < nLenOfSquareMaxf; iLenf++)
			} // for (iWidf = nWidOfSquareMinf; iWidf < nWidOfSquareMaxf; iWidf++)

		MarkContinueForiLenSquares: continue;
		} //for (iLenSquaresf = 0; iLenSquaresf < nNumOfSquaresInImageSidef; iLenSquaresf++)

	} // for (iWidSquaresf = 0; iWidSquaresf < nNumOfSquaresInImageSidef; iWidSquaresf++)

	fRatioOfSquaresf = (float)(nNumOfObjectSquaresTotf) / (float)(nNumOfSquaresInImageTotf);

	if (nNumOfObjectSquaresTotf > 1)
	{
		//fLogPointf = log(nNumOfObjectSquaresTotf) / (-log(nLenOfSquaref));
		fLogPointf = log(nNumOfObjectSquaresTotf);
	}
	else
		fLogPointf = 0.0;

#ifndef COMMENT_OUT_ALL_PRINTS
	printf( "\n\n The end of 'NumOfObjectSquaresAndLogPoint_WithResolution': nNumOfObjectSquaresTotf = %d, nNumOfSquaresInImageTotf = %d, fLogPointf = %E, fRatioOfSquaresf = %E",
		nNumOfObjectSquaresTotf, nNumOfSquaresInImageTotf, fLogPointf, fRatioOfSquaresf);
	printf("\n nLenOfSquaref = %d, nNumOfSquaresInImageSidef = %d", nLenOfSquaref, nNumOfSquaresInImageSidef);

	fprintf(fout_lr, "\n\n The end of 'NumOfObjectSquaresAndLogPoint_WithResolution': nNumOfObjectSquaresTotf = %d, nNumOfSquaresInImageTotf = %d, fLogPointf = %E, fRatioOfSquaresf = %E",
		nNumOfObjectSquaresTotf, nNumOfSquaresInImageTotf, fLogPointf, fRatioOfSquaresf);
	fprintf(fout_lr, "\n nLenOfSquaref = %d, nNumOfSquaresInImageSidef = %d", nLenOfSquaref, nNumOfSquaresInImageSidef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	return SUCCESSFUL_RETURN;
} //int NumOfObjectSquaresAndLogPoint_WithResolution(...
////////////////////////////////////////////////////////////////////////////////

//Tug-of-war lacunarity�A novel approach for estimating lacunarity --https://aip.scitation.org/doi/full/10.1063/1.4966539
int MassesOfAll_ObjectSquares_AtFixedLenOfSquare(
	const int nDim_2pNf,

	const int nLenOfSquaref,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	 int &nNumOfSquaresInImageTotf,

	int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf
	int &nMassOfImageTotf, //<= 

	int nMassesOfSquaresArrf[]) //[nNumOfSquaresTotf]


{
	int
		nIndexOfSquareCurf,

		nIndexOfPixelInEmbeddedImagef,

		nIndexOfPixelMaxf = nDim_2pNf * nDim_2pNf,

		iWidf,
		iLenf,

		nNumOfSquaresInImageSidef = nDim_2pNf / nLenOfSquaref,

		nMassOfASquaref, 
		nMassOfASquareMaxf = nLenOfSquaref * nLenOfSquaref,

		nWidOfSquareMinf,
		nWidOfSquareMaxf,

		nLenOfSquareMinf,
		nLenOfSquareMaxf,

		iWidSquaresf,
		iLenSquaresf;

	float
		fRatioOfSquaresf;
	////////////////////////////////////////////////
	nNumOfSquaresInImageTotf = nNumOfSquaresInImageSidef * nNumOfSquaresInImageSidef;

	nNumOfNonZero_ObjectSquaresTotf = 0;
	nMassOfImageTotf = 0;

#ifndef COMMENT_OUT_ALL_PRINTS
	nMassOfASquareMaxf = nLenOfSquaref * nLenOfSquaref,
		fprintf(fout_lr, "\n\n 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare' nDim_2pNf = %d, nNumOfSquaresInImageSidef = %d, nMassOfASquareMaxf = %d", 
			nDim_2pNf, nNumOfSquaresInImageSidef, nMassOfASquareMaxf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iWidSquaresf = 0; iWidSquaresf < nNumOfSquaresInImageSidef; iWidSquaresf++)
	{
		nWidOfSquareMinf = iWidSquaresf * nLenOfSquaref;
		nWidOfSquareMaxf = (iWidSquaresf + 1)* nLenOfSquaref;

		for (iLenSquaresf = 0; iLenSquaresf < nNumOfSquaresInImageSidef; iLenSquaresf++)
		{
			nIndexOfSquareCurf = iLenSquaresf + iWidSquaresf*nNumOfSquaresInImageSidef;
			if (nIndexOfSquareCurf >= nNumOfSquaresInImageTotf)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nIndexOfSquareCurf = %d >= nNumOfSquaresInImageTotf = %d", nIndexOfSquareCurf, nNumOfSquaresInImageTotf);
				fprintf(fout_lr, "\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nIndexOfSquareCurf = %d >= nNumOfSquaresInImageTotf = %d", nIndexOfSquareCurf, nNumOfSquaresInImageTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				return UNSUCCESSFUL_RETURN;
			}//if (nIndexOfSquareCurf >= nNumOfSquaresInImageTotf)

			nLenOfSquareMinf = iLenSquaresf * nLenOfSquaref;
			nLenOfSquareMaxf = (iLenSquaresf + 1)* nLenOfSquaref;

			if (nLenOfSquareMinf > sColor_ImageInitf->nLength || nWidOfSquareMinf > sColor_ImageInitf->nWidth)
			{
				nMassOfASquaref = 0;
				goto MarkContinueForiLenSquares;
			}//if (nLenOfSquareMinf > sColor_ImageInitf->nLength || nWidOfSquareMinf > sColor_ImageInitf->nWidth)

			nMassOfASquaref = 0;
			for (iWidf = nWidOfSquareMinf; iWidf < nWidOfSquareMaxf; iWidf++)
			{
				for (iLenf = nLenOfSquareMinf; iLenf < nLenOfSquareMaxf; iLenf++)
				{
					nIndexOfPixelInEmbeddedImagef = iLenf + (iWidf*nDim_2pNf);

					if (nIndexOfPixelInEmbeddedImagef >= nIndexOfPixelMaxf)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						printf("\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nIndexOfPixelInEmbeddedImagef = %d >= nIndexOfPixelMaxf = %d", nIndexOfPixelInEmbeddedImagef, nIndexOfPixelMaxf);
						fprintf(fout_lr, "\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nIndexOfPixelInEmbeddedImagef = %d >= nIndexOfPixelMaxf = %d", nIndexOfPixelInEmbeddedImagef, nIndexOfPixelMaxf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

						return UNSUCCESSFUL_RETURN;
					}//if (nIndexOfPixelInEmbeddedImagef >= nIndexOfPixelMaxf)

					if (sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] == 1)
					{
						nMassOfASquaref += 1;
					} //if (sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] == 1)

				} //for (iLenf = nLenOfSquareMinf
				
			} // for (iWidf = nWidOfSquareMinf; iWidf < nWidOfSquareMaxf; iWidf++)

			nMassOfImageTotf += nMassOfASquaref;

		MarkContinueForiLenSquares: nMassesOfSquaresArrf[nIndexOfSquareCurf] = nMassOfASquaref;
			if (nMassOfASquaref > 0)
			{
				nNumOfNonZero_ObjectSquaresTotf += 1;
			}//if (nMassOfASquaref > 0)

			if (nMassOfASquaref > nMassOfASquareMaxf)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nMassOfASquaref = %d > nMassOfASquareMaxf = %d", nMassOfASquaref, nMassOfASquareMaxf);
				printf( "\n iLenSquaresf = %d, iWidSquaresf = %d, nNumOfSquaresInImageSidef = %d", iLenSquaresf, iWidSquaresf, nNumOfSquaresInImageSidef);

				fprintf(fout_lr, "\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nMassOfASquaref = %d > nMassOfASquareMaxf = %d", nMassOfASquaref, nMassOfASquareMaxf);

				fprintf(fout_lr, "\n iLenSquaresf = %d, iWidSquaresf = %d, nNumOfSquaresInImageSidef = %d", iLenSquaresf, iWidSquaresf, nNumOfSquaresInImageSidef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				return UNSUCCESSFUL_RETURN;
			}//if (nMassOfASquaref > nMassOfASquareMaxf)
		} //for (iLenSquaresf = 0; iLenSquaresf < nNumOfSquaresInImageSidef; iLenSquaresf++)

	} // for (iWidSquaresf = 0; iWidSquaresf < nNumOfSquaresInImageSidef; iWidSquaresf++)

	if (nMassOfImageTotf < 0)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nMassOfImageTotf = %d < 0", nMassOfImageTotf);

		fprintf(fout_lr, "\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare':  nMassOfImageTotf = %d < 0", nMassOfImageTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		return UNSUCCESSFUL_RETURN;
	} // if (nMassOfImageTotf < 0)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n The end of 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nNumOfNonZero_ObjectSquaresTotf = %d, nNumOfSquaresInImageTotf = %d",
		nNumOfNonZero_ObjectSquaresTotf, nNumOfSquaresInImageTotf);

	printf("\n nLenOfSquaref = %d, nNumOfSquaresInImageSidef = %d, nMassOfImageTotf = %d", nLenOfSquaref, nNumOfSquaresInImageSidef, nMassOfImageTotf);

	fprintf(fout_lr, "\n\n The end of 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare':  nNumOfNonZero_ObjectSquaresTotf = %d, nNumOfSquaresInImageTotf = %d",
		nNumOfNonZero_ObjectSquaresTotf, nNumOfSquaresInImageTotf);

	fprintf(fout_lr, "\n nLenOfSquaref = %d, nNumOfSquaresInImageSidef = %d, nMassOfImageTotf = %d", nLenOfSquaref, nNumOfSquaresInImageSidef, nMassOfImageTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	return SUCCESSFUL_RETURN;
} //int MassesOfAll_ObjectSquares_AtFixedLenOfSquare(...
/////////////////////////////////////////////////////////////////////////////////////////

int ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare(
	
	const int nNumOfSquareOccurrence_Intervalsf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

	const int nDim_2pNf,

	const int nLenOfSquaref,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	//const int nNumOfSquaresTotf,
///////////////////////////////////////
	
	float fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[]) //[nNumOfSquareOccurrence_Intervalsf]
{
	int MassesOfAll_ObjectSquares_AtFixedLenOfSquare(
		const int nDim_2pNf,

		const int nLenOfSquaref,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		int &nNumOfSquaresInImageTotf,

		int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

		int &nMassOfImageTotf, //<= 

		int nMassesOfSquaresArrf[]); //[nNumOfSquaresInImageTotf]

	int
		iSquareOccurrenceIntervalf,
		iSquaref,

		iWidSquaresf,
		iLenSquaresf,
		nNumOfSquaresInImageSidef = nDim_2pNf / nLenOfSquaref,

		nNumOfSquaresInImagef, // = nNumOfSquaresInImageSidef* nNumOfSquaresInImageSidef,
		nMassOfImageTotf,

		nNumOfSquaresInImageTotf,
		nNumOfNonZero_ObjectSquaresTotf,

		nNumOfNonZero_ObjectSquaresCurf, 
		nMassOfASquaref,

		nMassOfASquareMaxf = nLenOfSquaref* nLenOfSquaref,

		nMassPerIntervalf,
		nIntervalOffSquareOccurrencef,

		nResf;
	/////////////////////////////

	nMassPerIntervalf = nMassOfASquareMaxf / nNumOfSquareOccurrence_Intervalsf;

	nNumOfSquaresInImagef = nNumOfSquaresInImageSidef * nNumOfSquaresInImageSidef;

	if (nNumOfSquaresInImagef <= 1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare': nNumOfSquaresInImagef = %d <= 1", nNumOfSquaresInImagef);
		fprintf(fout_lr, "\n\n An error in 'ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare': nNumOfSquaresInImagef = %d <= 1", nNumOfSquaresInImagef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		return UNSUCCESSFUL_RETURN;
	}//if (nNumOfSquaresInImagef <= 1)

	int *nMassesOfSquaresArrf;
	nMassesOfSquaresArrf = new int[nNumOfSquaresInImagef];

	if (nMassesOfSquaresArrf == nullptr)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in dynamic memory allocation for 'ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare'");
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nMassesOfSquaresArrf == nullptr)

	nResf = MassesOfAll_ObjectSquares_AtFixedLenOfSquare(
		nDim_2pNf, //const int nDim_2pNf,

		nLenOfSquaref, //const int nLenOfSquaref,

		sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

		sImageEmbeddedf_ForLacunarityBlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		nNumOfSquaresInImageTotf, //int &nNumOfSquaresInImageTotf,

		nNumOfNonZero_ObjectSquaresTotf, //int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

		nMassOfImageTotf, //int &nMassOfImageTotf, //<= 

		nMassesOfSquaresArrf); // int nMassesOfSquaresArrf[]); //[nNumOfSquaresTotf]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		delete[] nMassesOfSquaresArrf;
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

	if (nNumOfNonZero_ObjectSquaresTotf <= nNumOfNonZero_ObjectSquaresTotMin) //nNumOfSquareOccurrence_Intervalsf)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n Too few object squares in 'ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare': nNumOfNonZero_ObjectSquaresTotf = %d <= 1", nNumOfNonZero_ObjectSquaresTotf);

		fprintf(fout_lr, "\n\n Too few object squares in 'ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare': nNumOfNonZero_ObjectSquaresTotf = %d <= 1",	nNumOfNonZero_ObjectSquaresTotf);
		//printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		delete[] nMassesOfSquaresArrf;
		return (-2);
	}//if (nNumOfNonZero_ObjectSquaresTotf <= nNumOfNonZero_ObjectSquaresTotMin) //nNumOfSquareOccurrence_Intervalsf)

	for (iSquareOccurrenceIntervalf = 0; iSquareOccurrenceIntervalf < nNumOfSquareOccurrence_Intervalsf; iSquareOccurrenceIntervalf++)
	{
		fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iSquareOccurrenceIntervalf] = 0.0;
	} //for (iSquareOccurrenceIntervalf = 0; iSquareOccurrenceIntervalf < nNumOfSquareOccurrence_Intervalsf; iSquareOccurrenceIntervalf++)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'ProbOfMasses...': nLenOfSquaref = %d,  nNumOfSquaresInImagef = %d, nNumOfSquaresInImageTotf = %d,  nNumOfNonZero_ObjectSquaresTotf = %d, nMassPerIntervalf = %d",
		nLenOfSquaref, nNumOfSquaresInImagef, nNumOfSquaresInImageTotf, nNumOfNonZero_ObjectSquaresTotf, nMassPerIntervalf);

	fprintf(fout_lr, "\n\n 'ProbOfMasses...': nLenOfSquaref = %d,  nNumOfSquaresInImagef = %d, nNumOfSquaresInImageTotf = %d,  nNumOfNonZero_ObjectSquaresTotf = %d, nMassPerIntervalf = %d",
		nLenOfSquaref, nNumOfSquaresInImagef, nNumOfSquaresInImageTotf, nNumOfNonZero_ObjectSquaresTotf, nMassPerIntervalf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	if (nNumOfSquaresInImagef != nNumOfSquaresInImageTotf)
	{

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'ProbOfMasses...': nNumOfSquaresInImagef = %d != nNumOfSquaresInImageTotf = %d",
			nNumOfSquaresInImagef, nNumOfSquaresInImageTotf);
		fprintf(fout_lr, "\n\n An error in 'ProbOfMasses...': nNumOfSquaresInImagef = %d != nNumOfSquaresInImageTotf = %d",
			nNumOfSquaresInImagef, nNumOfSquaresInImageTotf);

		printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
	}//if (nNumOfSquaresInImagef != nNumOfSquaresInImageTotf)

	///////////////////////////////////////////////////////////////
	nNumOfNonZero_ObjectSquaresCurf = 0;
	for (iSquaref = 0; iSquaref < nNumOfSquaresInImageTotf; iSquaref++)
	{
		nMassOfASquaref = nMassesOfSquaresArrf[iSquaref];

		if (nMassOfASquaref > 0)
		{
			nNumOfNonZero_ObjectSquaresCurf += 1;

			nIntervalOffSquareOccurrencef = nMassOfASquaref / nMassPerIntervalf;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n iSquaref = %d, nNumOfSquaresInImageTotf = %d, nMassOfASquaref = %d, nIntervalOffSquareOccurrencef = %d, nMassPerIntervalf = %d",
				iSquaref, nNumOfSquaresInImageTotf, nMassOfASquaref, nIntervalOffSquareOccurrencef, nMassPerIntervalf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			if (nIntervalOffSquareOccurrencef < 0 || nIntervalOffSquareOccurrencef > nNumOfSquareOccurrence_Intervalsf)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'ProbOfMasses...': nIntervalOffSquareOccurrencef = %d > nNumOfSquareOccurrence_Intervalsf = %d, nLenOfSquaref = %d",
					nIntervalOffSquareOccurrencef, nNumOfSquareOccurrence_Intervalsf, nLenOfSquaref);
				fprintf(fout_lr, "\n\n An error in 'ProbOfMasses...': nIntervalOffSquareOccurrencef = %d > nNumOfSquareOccurrence_Intervalsf = %d, nLenOfSquaref = %d",
					nIntervalOffSquareOccurrencef, nNumOfSquareOccurrence_Intervalsf, nLenOfSquaref);

				printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				delete[] nMassesOfSquaresArrf;
				return UNSUCCESSFUL_RETURN;
			}//if (nIntervalOffSquareOccurrencef < 0 || nIntervalOffSquareOccurrencef > nNumOfSquareOccurrence_Intervalsf)

			if (nIntervalOffSquareOccurrencef == nNumOfSquareOccurrence_Intervalsf)
			{
#ifndef COMMENT_OUT_ALL_PRINTS

				fprintf(fout_lr, "\n\n 'ProbOfMasses...': nIntervalOffSquareOccurrencef = %d == nNumOfSquareOccurrence_Intervalsf = %d, iSquaref = %d",
					nIntervalOffSquareOccurrencef, nNumOfSquareOccurrence_Intervalsf, iSquaref);

				//printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[nNumOfSquareOccurrence_Intervalsf - 1] += 1.0;

				continue;
			}//if (nIntervalOffSquareOccurrencef < 0 || nIntervalOffSquareOccurrencef >= nNumOfSquareOccurrence_Intervalsf)

			fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[nIntervalOffSquareOccurrencef] += 1.0;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n iSquaref = %d, fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[%d] = %E, nLenOfSquaref = %d",
				iSquaref, nIntervalOffSquareOccurrencef, fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[nIntervalOffSquareOccurrencef], nLenOfSquaref);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		} //if (nMassOfASquaref > 0)

	}//for (iSquaref = 0; iSquaref < nNumOfSquaresInImageTotf; iSquaref++)

/////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
	for (iSquareOccurrenceIntervalf = 0; iSquareOccurrenceIntervalf < nNumOfSquareOccurrence_Intervalsf; iSquareOccurrenceIntervalf++)
	{
		fprintf(fout_lr, "\n\n Before normalizing: fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[%d] = %E, nLenOfSquaref = %d, nNumOfSquareOccurrence_Intervalsf = %d, nNumOfNonZero_ObjectSquaresTotf = %d",
			iSquareOccurrenceIntervalf, fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iSquareOccurrenceIntervalf], nLenOfSquaref, nNumOfSquareOccurrence_Intervalsf, nNumOfNonZero_ObjectSquaresTotf);
		fflush(fout_lr);
	} //for (iSquareOccurrenceIntervalf = 0; iSquareOccurrenceIntervalf < nNumOfSquareOccurrence_Intervalsf; iSquareOccurrenceIntervalf++)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	


	for (iSquareOccurrenceIntervalf = 0; iSquareOccurrenceIntervalf < nNumOfSquareOccurrence_Intervalsf; iSquareOccurrenceIntervalf++)
	{
		//fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iSquareOccurrenceIntervalf] = fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iSquareOccurrenceIntervalf] / (float)(nNumOfSquaresInImageTotf);
		fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iSquareOccurrenceIntervalf] = fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iSquareOccurrenceIntervalf] / (float)(nNumOfNonZero_ObjectSquaresTotf);

		if (fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iSquareOccurrenceIntervalf] < 0.0 || fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iSquareOccurrenceIntervalf] > 1.0)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'ProbOfMasses...': fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[%d] = %E, nNumOfNonZero_ObjectSquaresTotf = %d",
				iSquareOccurrenceIntervalf,fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iSquareOccurrenceIntervalf], nNumOfNonZero_ObjectSquaresTotf);

			fprintf(fout_lr, "\n\n An error in 'ProbOfMasses...': fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[%d] = %E, nNumOfNonZero_ObjectSquaresTotf = %d",
				iSquareOccurrenceIntervalf, fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iSquareOccurrenceIntervalf], nNumOfNonZero_ObjectSquaresTotf);

			printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			delete[] nMassesOfSquaresArrf;
			return UNSUCCESSFUL_RETURN;
		}//if (fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iSquareOccurrenceIntervalf] < 0.0 || fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iSquareOccurrenceIntervalf] > 1.0)

	} //for (iSquareOccurrenceIntervalf = 0; iSquareOccurrenceIntervalf < nNumOfSquareOccurrence_Intervalsf; iSquareOccurrenceIntervalf++)

	//printf("\n\n 'ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare' 1");
	delete[] nMassesOfSquaresArrf;
	return SUCCESSFUL_RETURN;
} // int ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare(...
/////////////////////////////////////////////////////////////
//Tug-of-war lacunarity�A novel approach for estimating lacunarity, https://aip.scitation.org/doi/full/10.1063/1.4966539
int LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare(

	const int nNumOfSquareOccurrence_Intervalsf,//4 <= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

	const int nDim_2pNf,

	const int nLenOfSquaref,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

///////////////////////////////////////
		float fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[], //[nNumOfSquareOccurrence_Intervalsf]

	float &fFirstMomentf,
	float &fSecondMomentf,

	float &fLacunarityf) 

{
	int ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare(

		const int nNumOfSquareOccurrence_Intervalsf,// 4 <= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

		const int nDim_2pNf,

		const int nLenOfSquaref,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		///////////////////////////////////////

		float fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[]); //[nNumOfSquareOccurrence_Intervalsf]

	int
		iSquareOccurrenceIntervalf,
		nResf;
	//////////////////////////////

		fFirstMomentf = 0.0;
		fSecondMomentf = 0.0;

/*
//nNumOfSquareOccurrence_Intervalsf == 4
	float *fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;
	fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf = new float[nNumOfSquareOccurrence_Intervalsf];

	if (fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf == nullptr)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in dynamic memory allocation for 'LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare'");
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare'");
		printf("\n\n Please press any key:"); getchar();
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf == nullptr)
*/

	nResf = ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare(

		nNumOfSquareOccurrence_Intervalsf, //const int nNumOfSquareOccurrence_Intervalsf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

		nDim_2pNf, //const int nDim_2pNf,

		nLenOfSquaref, //const int nLenOfSquaref,

		sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

		sImageEmbeddedf_ForLacunarityBlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]
		///////////////////////////////////////

		fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf); // float fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[]); //[nNumOfSquareOccurrence_Intervalsf]

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare' : after 'ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare'"); 
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		delete[] fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

	if (nResf == -2)
	{
		delete[] fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;
		return (-2);
	}//if (nResf == -2)
	/////////////////////

	for (iSquareOccurrenceIntervalf = 0; iSquareOccurrenceIntervalf < nNumOfSquareOccurrence_Intervalsf; iSquareOccurrenceIntervalf++)
	{
//midpoint of the interval
		fFirstMomentf += (float)( (float)(iSquareOccurrenceIntervalf)+0.5)*fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iSquareOccurrenceIntervalf];
		fSecondMomentf += (float)( (float)(iSquareOccurrenceIntervalf) + 0.5)*(float)( (float)(iSquareOccurrenceIntervalf)+0.5)*fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iSquareOccurrenceIntervalf];
	}//for (iSquareOccurrenceIntervalf = 0; iSquareOccurrenceIntervalf < nNumOfSquareOccurrence_Intervalsf; iSquareOccurrenceIntervalf++)

//	printf("\n\n 'LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare': 1");

	if (fFirstMomentf <= feps)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare': fFirstMomentf = %E <= feps", fFirstMomentf);
		fprintf(fout_lr, "\n\n An error in 'LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare': fFirstMomentf = %E <= feps", fFirstMomentf);
		printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		//delete[] fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;
		return UNSUCCESSFUL_RETURN;
	}//if (fFirstMomentf <= feps)

	fLacunarityf = fSecondMomentf / (fFirstMomentf*fFirstMomentf);

	//delete[] fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;
	return SUCCESSFUL_RETURN;
} //int LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare(...
//////////////////////////////////////////////////////////////////////////////////

//Tug-of-war lacunarity�A novel approach for estimating lacunarity, https://aip.scitation.org/doi/full/10.1063/1.4966539
int LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare(

	const int nNumOfSquareOccurrence_Intervalsf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

	const int nDim_2pNf,

	const int nNumOfLensOfSquareTot_Actualf,

	const int nLenOfSquareMinf,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

///////////////////////////////////////
		float fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf[], //[nNumOfLensOfSquareTot_Actualf*nNumOfSquareOccurrence_Intervalsf]

	float fFirstMoment_Arrf[], //[nNumOfLensOfSquareTot_Actualf]
	float fSecondMoment_Arrf[], //[nNumOfLensOfSquareTot_Actualf]

	float fLacunarity_Arrf[]) //[nNumOfLensOfSquareTot_Actualf]

{
	int LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare(

		const int nNumOfSquareOccurrence_Intervalsf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

		const int nDim_2pNf,

		const int nLenOfSquaref,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	///////////////////////////////////////
		float fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[], //[nNumOfSquareOccurrence_Intervalsf]

		float &fFirstMomentf,
		float &fSecondMomentf,

		float &fLacunarityf); //

	int
		nLenOfSquareCurf,

		nIndexOfSquaref,
		iNumOfSquaresf,

		nIndexOfProbMaxf = nNumOfLensOfSquareTot_Actualf * nNumOfSquareOccurrence_Intervalsf,
		nTempf, 
		iLenOfSquaref,
		nResf;

	float
		fFirstMomentf,
		fSecondMomentf,
		fLacunarityf;
	//////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare': nNumOfSquareOccurrence_Intervalsf = %d, nDim_2pNf = %d nNumOfLensOfSquareTot_Actualf = %d, nLenOfSquareMinf = %d",
		nNumOfSquareOccurrence_Intervalsf, nDim_2pNf, nNumOfLensOfSquareTot_Actualf, nLenOfSquareMinf);

	fprintf(fout_lr, "\n\n 'LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare': nNumOfSquareOccurrence_Intervalsf = %d, nDim_2pNf = %d nNumOfLensOfSquareTot_Actualf = %d, nLenOfSquareMinf = %d",
		nNumOfSquareOccurrence_Intervalsf, nDim_2pNf, nNumOfLensOfSquareTot_Actualf, nLenOfSquareMinf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	float *fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;
	fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf = new float[nNumOfSquareOccurrence_Intervalsf];

	if (fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf == nullptr)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in dynamic memory allocation for 'LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare'");
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare'");
		printf("\n\n Please press any key:"); getchar();
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf == nullptr)

	nLenOfSquareCurf = nDim_2pNf/ nDivisorForLenOfSquareMax_Init;
	for (iLenOfSquaref = 0; iLenOfSquaref < nNumOfLensOfSquareTot_Actualf; iLenOfSquaref++)
	{
		nLenOfSquareCurf = nLenOfSquareCurf / 2; //divisible by 2

		nTempf = nDim_2pNf / nLenOfSquareCurf;
		if (nNumOfSquareOccurrence_Intervalsf > nTempf*nTempf) //<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare': nNumOfSquareOccurrence_Intervalsf = %d > nTempf * nTempf = %d", 
				nNumOfSquareOccurrence_Intervalsf, nTempf* nTempf);

			fprintf(fout_lr, "\n\n An error in 'LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare': nNumOfSquareOccurrence_Intervalsf = %d > nTempf * nTempf = %d",
				nNumOfSquareOccurrence_Intervalsf, nTempf* nTempf);
			printf("\n\n Please press any key:"); getchar();
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			delete[] fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;
			return UNSUCCESSFUL_RETURN;
		}//if (nNumOfSquareOccurrence_Intervalsf > nTempf*nTempf)

		nResf = LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare(

			nNumOfSquareOccurrence_Intervalsf, //const int nNumOfSquareOccurrence_Intervalsf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

			nDim_2pNf, //const int nDim_2pNf,

			nLenOfSquareCurf, //const int nLenOfSquaref,

			sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

			sImageEmbeddedf_ForLacunarityBlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		///////////////////////////////////////
			fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf, //float fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[], //[nNumOfSquareOccurrence_Intervalsf]

			fFirstMomentf, //float &fFirstMomentf,
			fSecondMomentf, //float &fSecondMomentf,

			fLacunarityf); // float &fLacunarityf); //[nNumOfSquareOccurrence_Intervalsf]

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n After 'LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare', iLenOfSquaref = %d, nLenOfSquareCurf = %d, fFirstMomentf = %E, fSecondMomentf = %E, fLacunarityf = %E",
			iLenOfSquaref, nLenOfSquareCurf, fFirstMomentf, fSecondMomentf, fLacunarityf);	

		fprintf(fout_lr,"\n\n After 'LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare', iLenOfSquaref = %d, nLenOfSquareCurf = %d, fFirstMomentf = %E, fSecondMomentf = %E, fLacunarityf = %E",
				iLenOfSquaref, nLenOfSquareCurf, fFirstMomentf, fSecondMomentf, fLacunarityf);

#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			delete[] fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;
			return UNSUCCESSFUL_RETURN;
		}//if (nResf == UNSUCCESSFUL_RETURN)

		if (nResf == -2)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n Just one or no squares contain object pixels -- continue: iLenOfSquaref = %d, nLenOfSquareCurf = %d",
				iLenOfSquaref, nLenOfSquareCurf);

			fprintf(fout_lr, "\n\n Just one or no squares contain object pixels -- continue: iLenOfSquaref = %d, nLenOfSquareCurf = %d",
				iLenOfSquaref, nLenOfSquareCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			fFirstMoment_Arrf[iLenOfSquaref] = -1.0; //[nNumOfLensOfSquareTot_Actualf]
			fSecondMoment_Arrf[iLenOfSquaref] = -1.0; //[nNumOfLensOfSquareTot_Actualf]

			fLacunarity_Arrf[iLenOfSquaref] = -1.0;  //[nNumOfLensOfSquareTot_Actualf

			continue;
		}//if (nResf == -2)

		fFirstMoment_Arrf[iLenOfSquaref] = fFirstMomentf; //[nNumOfLensOfSquareTot_Actualf]
		fSecondMoment_Arrf[iLenOfSquaref] = fSecondMomentf; //[nNumOfLensOfSquareTot_Actualf]

		fLacunarity_Arrf[iLenOfSquaref] = fLacunarityf;  //[nNumOfLensOfSquareTot_Actualf

/////////////////////////////////////////////////////////////

		for (iNumOfSquaresf = 0; iNumOfSquaresf < nNumOfSquareOccurrence_Intervalsf; iNumOfSquaresf++)
		{
			//nIndexOfSquaref = iLenOfSquaref + (iNumOfSquaresf * nNumOfLenOfSquareMax);
			nIndexOfSquaref = iLenOfSquaref + (iNumOfSquaresf * nNumOfLensOfSquareTot_Actualf);

			if (nIndexOfSquaref >= nIndexOfProbMaxf)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare': nIndexOfSquaref = %d >= nIndexOfProbMaxf = %d", nIndexOfSquaref, nIndexOfProbMaxf);

				fprintf(fout_lr, "\n\n An error in LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare': nIndexOfSquaref = %d >= nIndexOfProbMaxf = %d", nIndexOfSquaref, nIndexOfProbMaxf);
				printf("\n\n Please press any key:"); getchar();

#endif // #ifndef COMMENT_OUT_ALL_PRINTS

				delete[] fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;
				return UNSUCCESSFUL_RETURN;
			} //if (nIndexOfSquaref >= nIndexOfProbMaxf)

		fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf[nIndexOfSquaref] = fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iNumOfSquaresf]; //[nNumOfLensOfSquareTot_Actualf*nNumOfSquareOccurrence_Intervalsf]

#ifndef COMMENT_OUT_ALL_PRINTS

		fprintf(fout_lr, "\n iLenOfSquaref = %d, iNumOfSquaresf = %d, fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf[%d] = %E",
			iLenOfSquaref, iNumOfSquaresf, nIndexOfSquaref, fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf[nIndexOfSquaref]);
		//fflush(fout_lr);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		}//for (iNumOfSquaresf = 0; iNumOfSquaresf < nNumOfSquareOccurrence_Intervalsf; iNumOfSquaresf++)
/////////////////////////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n iLenOfSquaref = %d, fFirstMoment_Arrf[%d] = %E, fSecondMoment_Arrf[%d] = %E, fLacunarity_Arrf[%d] = %E",
			iLenOfSquaref, iLenOfSquaref,fFirstMoment_Arrf[iLenOfSquaref], 
				iLenOfSquaref, fSecondMoment_Arrf[iLenOfSquaref], 
					iLenOfSquaref, fLacunarity_Arrf[iLenOfSquaref]);

		fprintf(fout_lr, "\n iLenOfSquaref = %d, fFirstMoment_Arrf[%d] = %E, fSecondMoment_Arrf[%d] = %E, fLacunarity_Arrf[%d] = %E",
			iLenOfSquaref, iLenOfSquaref, fFirstMoment_Arrf[iLenOfSquaref],
				iLenOfSquaref, fSecondMoment_Arrf[iLenOfSquaref],
					iLenOfSquaref, fLacunarity_Arrf[iLenOfSquaref]);

		fflush(fout_lr);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		if (nLenOfSquareCurf == nLenOfSquareMinf)
		{
			if (iLenOfSquaref != nNumOfLensOfSquareTot_Actualf - 1)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare': iLenOfSquaref = %d != nNumOfLensOfSquareTot_Actualf - 1 = %d at nLenOfSquareCurf == nLenOfSquareMinf", 
					iLenOfSquaref,nNumOfLensOfSquareTot_Actualf - 1);
				fprintf(fout_lr, "\n\n An error in 'LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare': iLenOfSquaref = %d != nNumOfLensOfSquareTot_Actualf - 1 = %d at nLenOfSquareCurf == nLenOfSquareMinf", 
					iLenOfSquaref, nNumOfLensOfSquareTot_Actualf - 1);
				printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				delete[] fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;

				return UNSUCCESSFUL_RETURN;
			} // if (iLenOfSquaref != nNumOfLensOfSquareTot_Actualf - 1)

			break;
		} //if (nLenOfSquareCurf == nLenOfSquareMinf)

	} //for (iLenOfSquaref = 0; iLenOfSquaref < nNumOfLensOfSquareTot_Actualf; iLenOfSquaref++)
	
#ifndef COMMENT_OUT_ALL_PRINTS

	printf("\n The end of 'LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare'");
	fprintf(fout_lr, "\n The end of 'LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare'");
	fflush(fout_lr);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	delete[] fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;

	return SUCCESSFUL_RETURN;
} //int LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare(...

////////////////////////////////////////////////////////////////////////
//Tug-of-war lacunarity�A novel approach for estimating lacunarity, https://aip.scitation.org/doi/full/10.1063/1.4966539
  //LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range
int LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range(

	const int nNumOfSquareOccurrence_Intervalsf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

	const int nDim_2pNf,

	const int nThresholdForIntensitiesMinf, //>=
	const int nThresholdForIntensitiesMaxf, // <

	const int nLenOfSquareMinf,

	const COLOR_IMAGE *sColor_ImageInitf,
	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

///////////////////////////////////////
	float fFeaAll_AtIntensity_Range_Arrf[], //[nNumOfFeasForLacunar_AtIntensity_Range] = [nNumOfLenOfSquareMax*3 + nNumOfLenOfSquareMax*nNumOfSquareOccurrence_Intervals]

	int &nNumOfLensOfSquareTot_Actualf)
{

int LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare(

	const int nNumOfSquareOccurrence_Intervalsf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

	const int nDim_2pNf,

	const int nNumOfLensOfSquareTot_Actualf,

	const int nLenOfSquareMinf,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

///////////////////////////////////////
		float fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf[], //[nNumOfLensOfSquareTot_Actualf*nNumOfSquareOccurrence_Intervalsf]

	float fFirstMoment_Arrf[], //[nNumOfLensOfSquareTot_Actualf]
	float fSecondMoment_Arrf[], //[nNumOfLensOfSquareTot_Actualf]

	float fLacunarity_Arrf[]); //[nNumOfLensOfSquareTot_Actualf]

	
	int
		nResf,

		iFeaf,
		nFeaf,

		nIndexOfSquaref,
		iNumOfSquaresf, // = nDim_2pNf / nLenOfSquaref, 

		nNumOfSquareOccurrence_IntervalsCurf,
		//nDim_2pNf,

		nScalef,
		nLenOfSquareCurf,

		nNumOfLenOfSquareCurf = 0,

		nIndexOfProbMaxf,
		nNumOfLensOfSquareTot_ForTestingf,
		iLenOfSquaref;

	//////////////////////////////
	//EMBEDDED_IMAGE_BLACK_WHITE sImageEmbeddedf_ForLacunarityBlackWhitef; //) //[nDim_2pNf*nDim_2pNf]
//////////////////////////////////

	//initialization
	for (iFeaf = 0; iFeaf < nNumOfFeasForLacunar_AtIntensity_Range; iFeaf++)
	{
		fFeaAll_AtIntensity_Range_Arrf[iFeaf] = 0.0;
	}//for (iFeaf = 0; iFeaf < nNumOfFeasForLacunar_AtIntensity_Range; iFeaf++)

////////////////////////////////////////////////////////
//verifying that 'nLenOfSquareCurf' == nNumOfLenOfSquareMax for 'nDim_2pN_Max'

	nLenOfSquareCurf = nDim_2pN_Max / nDivisorForLenOfSquareMax_Init;
	for (iLenOfSquaref = 0; iLenOfSquaref < nDim_2pN_Max; iLenOfSquaref++)
	{
		nLenOfSquareCurf = nLenOfSquareCurf / 2; //divisible by 2

		if (nLenOfSquareCurf == nLenOfSquareMinf) //nLenOfSquareMin == 2
		{
			nNumOfLensOfSquareTot_ForTestingf = iLenOfSquaref + 1;

#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': iLenOfSquaref = %d, nNumOfLensOfSquareTot_ForTestingf = %d",
				iLenOfSquaref, nNumOfLensOfSquareTot_ForTestingf);

			fprintf(fout_lr, "\n\n 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': iLenOfSquaref = %d, nNumOfLensOfSquareTot_ForTestingf = %d",
				iLenOfSquaref, nNumOfLensOfSquareTot_ForTestingf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			break;
		} //if (nLenOfSquareCurf == 1)

	} //for (iLenOfSquaref = 0; iLenOfSquaref < nDim_2pN_Max; iLenOfSquaref++)

	//printf("\n 3"); getchar();

	if (nNumOfLensOfSquareTot_ForTestingf != nNumOfLenOfSquareMax)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': nNumOfLensOfSquareTot_ForTestingf = %d != nNumOfLenOfSquareMax = %d", 
			nNumOfLensOfSquareTot_ForTestingf, nNumOfLenOfSquareMax);
		fprintf(fout_lr, "\n\n An error in 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': nNumOfLensOfSquareTot_ForTestingf = %d != nNumOfLenOfSquareMax = %d",
			nNumOfLensOfSquareTot_ForTestingf, nNumOfLenOfSquareMax);
		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		return UNSUCCESSFUL_RETURN;
	}//if (nLenOfSquareCurf != nNumOfLenOfSquareMax)
//////////////////////////////////
	//finding 'nNumOfLensOfSquareTot_Actualf'
	nNumOfLensOfSquareTot_Actualf = 0;

	nLenOfSquareCurf = nDim_2pNf/ nDivisorForLenOfSquareMax_Init; //nDivisorForLenOfSquareMax_Init == 2
	for (iLenOfSquaref = 0; iLenOfSquaref < nDim_2pNf; iLenOfSquaref++)
	{
		nLenOfSquareCurf = nLenOfSquareCurf / 2; //divisible by 2

		if (nLenOfSquareCurf == nLenOfSquareMinf)
		{
			nNumOfLensOfSquareTot_Actualf = iLenOfSquaref + 1;

#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': iLenOfSquaref = %d, nNumOfLensOfSquareTot_Actualf = %d",
					iLenOfSquaref, nNumOfLensOfSquareTot_Actualf);

				fprintf(fout_lr, "\n\n 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': iLenOfSquaref = %d, nNumOfLensOfSquareTot_Actualf = %d",
					iLenOfSquaref, nNumOfLensOfSquareTot_Actualf);
				
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			break;
		} //if (nLenOfSquareCurf == 1)

	} //for (iLenOfSquaref = 0; iLenOfSquaref < nDim_2pNf; iLenOfSquaref++)

	if (nNumOfLensOfSquareTot_Actualf == 0)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': nNumOfLensOfSquareTot_Actualf == 0");
		printf( "\n nDim_2pNf = %d, nLenOfSquareMinf = %d", nDim_2pNf, nLenOfSquareMinf);


		fprintf(fout_lr, "\n\n An error in 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': nNumOfLensOfSquareTot_Actualf == 0");

		fprintf(fout_lr, "\n nDim_2pNf = %d, nLenOfSquareMinf = %d", nDim_2pNf, nLenOfSquareMinf);
		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		return UNSUCCESSFUL_RETURN;
	} //if (nNumOfLensOfSquareTot_Actualf == 0)
///////////////////////////////////////

	float *fFirstMoment_Arrf; //[nNumOfLensOfSquareTot_Actualf]
	fFirstMoment_Arrf = new float[nNumOfLensOfSquareTot_Actualf];

	if (fFirstMoment_Arrf == nullptr)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in dynamic memory allocation for 'fFirstMoment_Arrf' in LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range'");
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'fFirstMoment_Arrf' in 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range'");
		printf("\n\n Please press any key:"); fflush(fout_lr);  getchar();

#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (fFirstMoment_Arrf == nullptr)
///////////////////////////////////////////
	float *fSecondMoment_Arrf; //[nNumOfLensOfSquareTot_Actualf]
	fSecondMoment_Arrf = new float[nNumOfLensOfSquareTot_Actualf];

	if (fSecondMoment_Arrf == nullptr)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in dynamic memory allocation for 'fSecondMoment_Arrf' in LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range'");
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'fSecondMoment_Arrf' in 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range'");
		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();

#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		delete[] fFirstMoment_Arrf;
		return UNSUCCESSFUL_RETURN;
	} //if (fSecondMoment_Arrf == nullptr)
///////////////////////////////////////////////////

	float *fLacunarity_Arrf; //[nNumOfLensOfSquareTot_Actualf]
	fLacunarity_Arrf = new float[nNumOfLensOfSquareTot_Actualf];

	if (fLacunarity_Arrf == nullptr)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in dynamic memory allocation for 'fLacunarity_Arrf' in LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range'");
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'fLacunarity_Arrf' in 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range'");
		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();

#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		delete[] fFirstMoment_Arrf;
		delete[] fSecondMoment_Arrf;
		return UNSUCCESSFUL_RETURN;
	} //if (fLacunarity_Arrf == nullptr)
//////////////////////////////////////////////////

	nIndexOfProbMaxf = nNumOfLensOfSquareTot_Actualf * nNumOfSquareOccurrence_Intervalsf;
	float *fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf; //
	fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf = new float[nIndexOfProbMaxf];

	if (fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf == nullptr)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in dynamic memory allocation for 'fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf' in LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range'");
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf' in 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range'");
		printf("\n\n Please press any key:"); fflush(fout_lr);  getchar();

#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		delete[] fFirstMoment_Arrf;
		delete[] fSecondMoment_Arrf;
		delete[] fLacunarity_Arrf;

		return UNSUCCESSFUL_RETURN;
	} //if (fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf == nullptr)
//////////////////////////////////////////////
	//printf("\n 5"); //getchar();

		nResf = LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare(

			nNumOfSquareOccurrence_Intervalsf, //const int nNumOfSquareOccurrence_Intervalsf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquareCurf)*(nDim_2pNf / nLenOfSquareCurf)

			nDim_2pNf, //const int nDim_2pNf,

			nNumOfLensOfSquareTot_Actualf, //const int nNumOfLensOfSquareTot_Actualf,

			nLenOfSquareMinf, //const int nLenOfSquareMinf,

			sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

			sImageEmbeddedf_ForLacunarityBlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		///////////////////////////////////////
				fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf, //float fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf[], //[nNumOfLensOfSquareTot_Actualf*nNumOfSquareOccurrence_Intervalsf]

			fFirstMoment_Arrf, //float fFirstMoment_Arrf[], //[nNumOfLensOfSquareTot_Actualf]
			fSecondMoment_Arrf, //float fSecondMoment_Arrf[], //[nNumOfLensOfSquareTot_Actualf]

			fLacunarity_Arrf); // float fLacunarity_Arrf[]); //[nNumOfLensOfSquareTot_Actualf]

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			delete[] fFirstMoment_Arrf;
			delete[] fSecondMoment_Arrf;
			delete[] fLacunarity_Arrf;
			delete[] fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf;

			return UNSUCCESSFUL_RETURN;
		}//if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
		//fprintf(fout_lr, "\n\n 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': the feas\n");
		//printf("\n\n Please press any key:"); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		//printf("\n 6");// getchar();

///////////////////////////////////////////////////////////////
//fFeaAll_AtIntensity_Range_Arrf[], //[nNumOfFeasForLacunar_AtIntensity_Range] = [nNumOfLenOfSquareMax*3 + nNumOfLenOfSquareMax*nNumOfSquareOccurrence_Intervals]

		nFeaf = 0;
		for (iLenOfSquaref = 0; iLenOfSquaref < nNumOfLensOfSquareTot_Actualf; iLenOfSquaref++)
		{
			fFeaAll_AtIntensity_Range_Arrf[nFeaf] = fFirstMoment_Arrf[iLenOfSquaref];

#ifndef COMMENT_OUT_ALL_PRINTS
			//fprintf(fout_lr, "%d:%E \n", nFeaf, fFeaAll_AtIntensity_Range_Arrf[nFeaf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			nFeaf += 1;
		} //for (iLenOfSquaref = 0; iLenOfSquaref < nNumOfLensOfSquareTot_Actualf; iLenOfSquaref++)
/////////////////////////////////////
		nFeaf = nNumOfLenOfSquareMax;

		for (iLenOfSquaref = 0; iLenOfSquaref < nNumOfLensOfSquareTot_Actualf; iLenOfSquaref++)
		{
			fFeaAll_AtIntensity_Range_Arrf[nFeaf] = fSecondMoment_Arrf[iLenOfSquaref];

#ifndef COMMENT_OUT_ALL_PRINTS
			//fprintf(fout_lr, "%d:%E \n", nFeaf, fFeaAll_AtIntensity_Range_Arrf[nFeaf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			nFeaf += 1;
		} //for (iLenOfSquaref = 0; iLenOfSquaref < nNumOfLensOfSquareTot_Actualf; iLenOfSquaref++)
///////////////////////////////////////////
		nFeaf = 2*nNumOfLenOfSquareMax;

		for (iLenOfSquaref = 0; iLenOfSquaref < nNumOfLensOfSquareTot_Actualf; iLenOfSquaref++)
		{
			fFeaAll_AtIntensity_Range_Arrf[nFeaf] = fLacunarity_Arrf[iLenOfSquaref];
#ifndef COMMENT_OUT_ALL_PRINTS
			//fprintf(fout_lr, "%d:%E \n", nFeaf, fFeaAll_AtIntensity_Range_Arrf[nFeaf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			nFeaf += 1;
		} //for (iLenOfSquaref = 0; iLenOfSquaref < nNumOfLensOfSquareTot_Actualf; iLenOfSquaref++)
/////////////////////////////////////////
		nFeaf = 3 * nNumOfLenOfSquareMax; // nNumOfLenOfSquareMax == 7
//fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf, //float fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf[], //[nNumOfLensOfSquareTot_Actualf*nNumOfSquareOccurrence_Intervalsf]

		for (iNumOfSquaresf = 0; iNumOfSquaresf < nNumOfSquareOccurrence_Intervalsf; iNumOfSquaresf++)
		{
			for (iLenOfSquaref = 0; iLenOfSquaref < nNumOfLensOfSquareTot_Actualf; iLenOfSquaref++)
			{
				//nIndexOfSquaref = iLenOfSquaref + (iNumOfSquaresf * nNumOfLenOfSquareMax);
				nIndexOfSquaref = iLenOfSquaref + (iNumOfSquaresf * nNumOfLensOfSquareTot_Actualf);

				if (nIndexOfSquaref >= nIndexOfProbMaxf)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\n An error in LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': nIndexOfSquaref = %d >= nIndexOfProbMaxf = %d",	nIndexOfSquaref, nIndexOfProbMaxf);

					fprintf(fout_lr, "\n\n An error in LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': nIndexOfSquaref = %d >= nIndexOfProbMaxf = %d", nIndexOfSquaref, nIndexOfProbMaxf);
					printf("\n\n Please press any key:"); getchar();

#endif // #ifndef COMMENT_OUT_ALL_PRINTS

					delete[] fFirstMoment_Arrf;
					delete[] fSecondMoment_Arrf;
					delete[] fLacunarity_Arrf;
					delete[] fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf;

					return UNSUCCESSFUL_RETURN;
				} //if (nIndexOfSquaref >= nIndexOfProbMaxf)

				if (nFeaf >= nNumOfFeasForLacunar_AtIntensity_Range)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\n An error in LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': nFeaf = %d >= nNumOfFeasForLacunar_AtIntensity_Range = %d", 
						nFeaf, nNumOfFeasForLacunar_AtIntensity_Range);
					fprintf(fout_lr, "\n\n An error in LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': nFeaf = %d >= nNumOfFeasForLacunar_AtIntensity_Range = %d", 
						nFeaf, nNumOfFeasForLacunar_AtIntensity_Range);
					printf("\n\n Please press any key:"); getchar();

#endif // #ifndef COMMENT_OUT_ALL_PRINTS

					delete[] fFirstMoment_Arrf;
					delete[] fSecondMoment_Arrf;
					delete[] fLacunarity_Arrf;
					delete[] fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf;

					return UNSUCCESSFUL_RETURN;
				}//if (nFeaf >= nNumOfFeasForLacunar_AtIntensity_Range)

				fFeaAll_AtIntensity_Range_Arrf[nFeaf] = fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf[nIndexOfSquaref];

#ifndef COMMENT_OUT_ALL_PRINTS
				//fprintf(fout_lr, "%d:%E \n", nFeaf, fFeaAll_AtIntensity_Range_Arrf[nFeaf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				nFeaf += 1;
			} //for (iLenOfSquaref = 0; iLenOfSquaref < nNumOfLensOfSquareTot_Actualf; iLenOfSquaref++)

		}//for (iNumOfSquaresf = 0; iNumOfSquaresf < nNumOfSquareOccurrence_Intervalsf; iNumOfSquaresf++)

		//printf("\n 7"); getchar();

///////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': nNumOfSquareOccurrence_Intervalsf = %d, nDim_2pNf = %d nNumOfLensOfSquareTot_Actualf = %d, nLenOfSquareMinf = %d",
		nNumOfSquareOccurrence_Intervalsf, nDim_2pNf, nNumOfLensOfSquareTot_Actualf, nLenOfSquareMinf);

	fprintf(fout_lr, "\n\n 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': nNumOfSquareOccurrence_Intervalsf = %d, nDim_2pNf = %d nNumOfLensOfSquareTot_Actualf = %d, nLenOfSquareMinf = %d",
		nNumOfSquareOccurrence_Intervalsf, nDim_2pNf, nNumOfLensOfSquareTot_Actualf, nLenOfSquareMinf);
	fflush(fout_lr);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	delete[] fFirstMoment_Arrf;
	delete[] fSecondMoment_Arrf;
	delete[] fLacunarity_Arrf;

	delete[] fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf;

	#ifndef COMMENT_OUT_ALL_PRINTS	
	//printf("\n\n The end of 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': please press any key");
	//fprintf(fout_lr, "\n\n The end of 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': please press any key"); fflush(fout_lr); getchar();

	printf("\n\n The end of 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range'");

	fprintf(fout_lr, "\n\n The end of 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range'"); fflush(fout_lr); 

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	return SUCCESSFUL_RETURN;
} //int LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range(...
////////////////////////////////////////////////////////////////////

int LacunarityOfMasses_At_All_Intensity_Ranges(

	const int nNumOfSquareOccurrence_Intervalsf,// == 4 <= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

	//const int nDim_2pNf,

	const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,
	const int nLenOfSquareMinf,

	const COLOR_IMAGE *sColor_ImageInitf,
	//const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

///////////////////////////////////////
	float fOneDim_Lacunar_Arrf[]) //[nNumOfLacunarFeasFor_OneDimTot] //nNumOfLacunarFeasFor_OneDimTot = nNumOfFeasForLacunar_AtIntensity_Range*nNumOfIntensity_IntervalsForLacunarTot

//int &nNumOfLensOfSquareTot_Actualf)
{
	int Dim_2powerN(
		const int nLengthf,
		const int nWidthf,

		int &nScalef,

		int &nDim_2pNf);

	int Initializing_Embedded_Image(
		const int nDim_2pNf,

		EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef); // //[nDim_2pNf*nDim_2pNf]

	int Embedding_Image_Into_2powerN_ForLacunarity(
		const int nDim_2pNf,

		const int nThresholdForIntensitiesMinf, //>=
		const int nThresholdForIntensitiesMaxf, // <

		const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		const COLOR_IMAGE *sColor_ImageInitf,
		EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef); //[nDim_2pNf*nDim_2pNf]

	int LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range(

		const int nNumOfSquareOccurrence_Intervalsf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

		const int nDim_2pNf,

		const int nThresholdForIntensitiesMinf, //>=
		const int nThresholdForIntensitiesMaxf, // <

		const int nLenOfSquareMinf,

		const COLOR_IMAGE *sColor_ImageInitf,
		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	///////////////////////////////////////
		float fFeaAll_AtIntensity_Range_Arrf[], //[nNumOfFeasForLacunar_AtIntensity_Range] = [nNumOfLenOfSquareMax*3 + nNumOfLenOfSquareMax*nNumOfSquareOccurrence_Intervals]

		int &nNumOfLensOfSquareTot_Actualf);

	int
		nResf,
		nLenOfIntensityIntervalf = (nIntensityStatMax + 1) / nNumOfIntensity_IntervalsForLacunar, // 8
		
		nScalef,
		nDim_2pNf,

		nThresholdForIntensitiesMinf,
		nThresholdForIntensitiesMaxf,

		nNumOfLensOfSquareTot_Actualf,

		nTempf,
		iFeaf,
		iIntensity_Interval_1f,
		iIntensity_Interval_2f,

		nFea_Curf,
		nNumOfIntensity_IntervalsForLacunarTotf;

	float
		fFeaAll_AtIntensity_Range_Arrf[nNumOfFeasForLacunar_AtIntensity_Range];
/////////////////////////////

	EMBEDDED_IMAGE_BLACK_WHITE sImageEmbeddedf_ForLacunarityBlackWhitef; //) //[nDim_2pNf*nDim_2pNf]
/////////////////////////////////////////////////
//initialization
	for (iFeaf = 0; iFeaf < nNumOfLacunarFeasFor_OneDimTot; iFeaf++)
	{
		fOneDim_Lacunar_Arrf[iFeaf] = 0.0;
	}//for (iFeaf = 0; iFeaf < nNumOfLacunarFeasFor_OneDimTot; iFeaf++)
//////////////

	nResf = Dim_2powerN(
		sColor_ImageInitf->nLength, //const int nLengthf,
		sColor_ImageInitf->nWidth, //const int nWidthf,

		nScalef, //int &nScalef,

		nDim_2pNf); // int &nDim_2pNf);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'LacunarityOfMasses_At_All_Intensity_Ranges': nScalef = %d, nDim_2pNf = %d, nNumOfIntensity_IntervalsForLacunarTot = %d",
		nScalef, nDim_2pNf, nNumOfIntensity_IntervalsForLacunarTot);

	fprintf(fout_lr, "\n\n 'LacunarityOfMasses_At_All_Intensity_Ranges':nScalef = %d, nDim_2pNf = %d, nNumOfIntensity_IntervalsForLacunarTot = %d",
		nScalef, nDim_2pNf, nNumOfIntensity_IntervalsForLacunarTot);

	fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	if (nScalef <= 1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'LacunarityOfMasses_At_All_Intensity_Ranges': nScalef = %d", nScalef);
		fprintf(fout_lr, "\n\n An error in 'LacunarityOfMasses_At_All_Intensity_Ranges': nScalef = %d", nScalef);
		printf("\n\n Please press any key:"); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		return UNSUCCESSFUL_RETURN;
	}//if (nScalef <= 1)
	//////////////////////////////////////////////

	nResf = Initializing_Embedded_Image(
		nDim_2pNf, //const int nDim_2pNf,

		&sImageEmbeddedf_ForLacunarityBlackWhitef); // EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef) // //[nDim_2pNf*nDim_2pNf]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

	///////////////////////////////////////
	nNumOfIntensity_IntervalsForLacunarTotf = 0;
	for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForLacunar; iIntensity_Interval_1f++)
	{
		nThresholdForIntensitiesMinf = iIntensity_Interval_1f * nLenOfIntensityIntervalf;

		//iIntensity_Interval_1_Glob = iIntensity_Interval_1f;
		for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForLacunar; iIntensity_Interval_2f++)
		{
			//iIntensity_Interval_2_Glob = iIntensity_Interval_2f;

			nNumOfIntensity_IntervalsForLacunarTotf += 1;

			if (nNumOfIntensity_IntervalsForLacunarTotf > nNumOfIntensity_IntervalsForLacunarTot)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'LacunarityOfMasses_At_All_Intensity_Ranges': nNumOfIntensity_IntervalsForLacunarTotf = %d > nNumOfIntensity_IntervalsForLacunarTot = %d",
					nNumOfIntensity_IntervalsForLacunarTotf, nNumOfIntensity_IntervalsForLacunarTot);

				printf("\n iIntensity_Interval_1f = %d, iIntensity_Interval_2f = %d", iIntensity_Interval_1f, iIntensity_Interval_2f);

				fprintf(fout_lr, "\n\n An error in 'LacunarityOfMasses_At_All_Intensity_Ranges': nNumOfIntensity_IntervalsForLacunarTotf = %d > nNumOfIntensity_IntervalsForLacunarTot = %d",
					nNumOfIntensity_IntervalsForLacunarTotf, nNumOfIntensity_IntervalsForLacunarTot);

				fprintf(fout_lr, "\n iIntensity_Interval_1f = %d, iIntensity_Interval_2f = %d", iIntensity_Interval_1f, iIntensity_Interval_2f);
				printf("\n\n Please press any key:"); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
			} //if (nNumOfIntensity_IntervalsForLacunarTotf > nNumOfIntensity_IntervalsForLacunarTot)

			nThresholdForIntensitiesMaxf = iIntensity_Interval_2f * nLenOfIntensityIntervalf;

#ifndef COMMENT_OUT_ALL_PRINTS	
			fprintf(fout_lr, "\n\n 1: sWeightsOfColorsf->fWeightOfRed = %E, sWeightsOfColorsf->fWeightOfGreen = %E, sWeightsOfColorsf->fWeightOfBlue = %E",
				sWeightsOfColorsf->fWeightOfRed, sWeightsOfColorsf->fWeightOfGreen, sWeightsOfColorsf->fWeightOfBlue);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			nResf = Embedding_Image_Into_2powerN_ForHausdorff(
				nDim_2pNf, //const int nDim_2pNf,

				nThresholdForIntensitiesMinf, //const int nThresholdForIntensitiesMinf,
				nThresholdForIntensitiesMaxf, //const int nThresholdForIntensitiesMaxf,

				sWeightsOfColorsf, //const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

				sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,
				&sImageEmbeddedf_ForLacunarityBlackWhitef); // EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef) //[nDim_2pNf*nDim_2pNf]

			if (nResf == UNSUCCESSFUL_RETURN)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'LacunarityOfMasses_At_All_Intensity_Ranges': nScalef = %d", nScalef);

				fprintf(fout_lr, "\n\n An error in 'LacunarityOfMasses_At_All_Intensity_Ranges': nScalef = %d", nScalef);
				printf("\n\n Please press any key:"); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				return UNSUCCESSFUL_RETURN;
			}//if (nResf == UNSUCCESSFUL_RETURN)

		///	fprintf(fout_lr, "\n\n2: sWeightsOfColorsf->fWeightOfRed = %E, sWeightsOfColorsf->fWeightOfGreen = %E, sWeightsOfColorsf->fWeightOfBlue = %E",
			//	sWeightsOfColorsf->fWeightOfRed, sWeightsOfColorsf->fWeightOfGreen, sWeightsOfColorsf->fWeightOfBlue);

			nResf = LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range(

				nNumOfSquareOccurrence_Intervalsf, //const int nNumOfSquareOccurrence_Intervalsf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

				nDim_2pNf, //const int nDim_2pNf,

				nThresholdForIntensitiesMinf, //const int nThresholdForIntensitiesMinf, //>=
				nThresholdForIntensitiesMaxf, //const int nThresholdForIntensitiesMaxf, // <

				nLenOfSquareMinf, //const int nLenOfSquareMinf,

				sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,
				&sImageEmbeddedf_ForLacunarityBlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

			///////////////////////////////////////
				fFeaAll_AtIntensity_Range_Arrf, // float fFeaAll_AtIntensity_Range_Arrf[], //[nNumOfFeasForLacunar_AtIntensity_Range] = [nNumOfLenOfSquareMax*3 + nNumOfLenOfSquareMax*nNumOfSquareOccurrence_Intervals]

				nNumOfLensOfSquareTot_Actualf); // int &nNumOfLensOfSquareTot_Actualf);

////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS	

			fprintf(fout_lr, "\n\n nNumOfIntensity_IntervalsForLacunarTotf = %d, iIntensity_Interval_1f = %d, iIntensity_Interval_2f = %d",
				nNumOfIntensity_IntervalsForLacunarTotf, iIntensity_Interval_1f, iIntensity_Interval_2f);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			nTempf = (nNumOfIntensity_IntervalsForLacunarTotf - 1) * nNumOfFeasForLacunar_AtIntensity_Range;
			for (iFeaf = 0; iFeaf < nNumOfFeasForLacunar_AtIntensity_Range; iFeaf++)
			{
				nFea_Curf = iFeaf + nTempf;

#ifndef COMMENT_OUT_ALL_PRINTS	
				//fprintf(fout_lr, "\n fFeaAll_AtIntensity_Range_Arrf[%d] = %E, nFea_Curf = %d, nTempf = %d", iFeaf, fFeaAll_AtIntensity_Range_Arrf[iFeaf], nFea_Curf, nTempf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				if (nFea_Curf >= nNumOfLacunarFeasFor_OneDimTot)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\n An error in 'LacunarityOfMasses_At_All_Intensity_Ranges': nFea_Curf = %d >= nNumOfLacunarFeasFor_OneDimTot = %d", nFea_Curf, nNumOfLacunarFeasFor_OneDimTot);
					fprintf(fout_lr, "\n\n An error in 'LacunarityOfMasses_At_All_Intensity_Ranges': nFea_Curf = %d >= nNumOfLacunarFeasFor_OneDimTot = %d", 
						nFea_Curf, nNumOfLacunarFeasFor_OneDimTot);
					printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					return UNSUCCESSFUL_RETURN;
				}//if (nFea_Curf >= nNumOfLacunarFeasFor_OneDimTot)

				fOneDim_Lacunar_Arrf[nFea_Curf] = fFeaAll_AtIntensity_Range_Arrf[iFeaf];

#ifndef COMMENT_OUT_ALL_PRINTS	
				fprintf(fout_lr, "\n fOneDim_Lacunar_Arrf[%d] = %E, nFea_Curf = %d, nTempf = %d", nFea_Curf, fOneDim_Lacunar_Arrf[nFea_Curf], nFea_Curf, nTempf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			}//for (iFeaf = 0; iFeaf < nNumOfFeasForLacunar_AtIntensity_Range; iFeaf++)

			//fprintf(fout_lr, "\n\n4: sWeightsOfColorsf->fWeightOfRed = %E, sWeightsOfColorsf->fWeightOfGreen = %E, sWeightsOfColorsf->fWeightOfBlue = %E",
				//sWeightsOfColorsf->fWeightOfRed, sWeightsOfColorsf->fWeightOfGreen, sWeightsOfColorsf->fWeightOfBlue);

		} //for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForLacunar; iIntensity_Interval_2f++)

	}//for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForLacunar; iIntensity_Interval_1f++)

///////////////
return SUCCESSFUL_RETURN;
}//int LacunarityOfMasses_At_All_Intensity_Ranges(...


//////////////////////////////////////////////////////////////////////////////////////////
int SetOfLogPoints_ForFractal_Dim(
	const int nThresholdForIntensitiesMinf,
	const int nThresholdForIntensitiesMaxf,
	
	const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		const COLOR_IMAGE *sColor_ImageInitf,

		int &nNumOfLogPointsf,
		
		float fNegLogOfLenOfSquare_Arrf[], //[nNumOfIters_ForDim_2powerN_Max]
		float fLogPoints_Arrf[]) // [nNumOfIters_ForDim_2powerN_Max]
{
	int Dim_2powerN(
		const int nLengthf,
		const int nWidthf,

		int &nScalef,

		int &nDim_2pNf);

	int Initializing_Embedded_Image(
		const int nDim_2pNf,

		EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef); // //[nDim_2pNf*nDim_2pNf]

	int Embedding_Image_Into_2powerN_ForHausdorff(
		const int nDim_2pNf,

		const int nThresholdForIntensitiesMinf,
		const int nThresholdForIntensitiesMaxf,

		const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		const COLOR_IMAGE *sColor_ImageInitf,
		EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef); //[nDim_2pNf*nDim_2pNf]

	int NumOfObjectSquaresAndLogPoint_WithResolution(
		const int nDim_2pNf,

		const int nLenOfSquaref,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		int &nNumOfObjectSquaresTotf,
		float &fLogPointf);

	int
		nResf,
		iScalef,
		nScalef = 1,

		nNumOfObjectSquaresTotf,

		nDim_2pNf,
		nLenOfSquaref;

	float
		fLogPointf;

	EMBEDDED_IMAGE_BLACK_WHITE sImageEmbeddedf_ForLacunarityBlackWhitef; //) //[nDim_2pNf*nDim_2pNf]
//////////////
	
nResf = Dim_2powerN(
	sColor_ImageInitf->nLength, //const int nLengthf,
	sColor_ImageInitf->nWidth, //const int nWidthf,

	nScalef, //int &nScalef,

	nDim_2pNf); // int &nDim_2pNf);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'SetOfLogPoints_ForFractal_Dim': nScalef = %d, nDim_2pNf = %d", nScalef, nDim_2pNf);
	fprintf(fout_lr, "\n\n 'SetOfLogPoints_ForFractal_Dim': nScalef = %d, nDim_2pNf = %d", nScalef, nDim_2pNf);

	fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	if (nScalef <= 1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'SetOfLogPoints_ForFractal_Dim': nScalef = %d", nScalef);
		fprintf(fout_lr, "\n\n An error in 'SetOfLogPoints_ForFractal_Dim': nScalef = %d", nScalef);
		printf("\n\n Please press any key:"); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		return UNSUCCESSFUL_RETURN;
	}//if (nScalef <= 1)
	//////////////////////////////////////////////

	nResf = Initializing_Embedded_Image(
		nDim_2pNf, //const int nDim_2pNf,

		&sImageEmbeddedf_ForLacunarityBlackWhitef); // EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef) // //[nDim_2pNf*nDim_2pNf]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

	//printf("\n 5"); getchar();

	nResf = Embedding_Image_Into_2powerN_ForHausdorff(
		nDim_2pNf, //const int nDim_2pNf,

		nThresholdForIntensitiesMinf, //const int nThresholdForIntensitiesMinf,
		nThresholdForIntensitiesMaxf, //const int nThresholdForIntensitiesMaxf,

		sWeightsOfColorsf, //const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,
		&sImageEmbeddedf_ForLacunarityBlackWhitef); // EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef) //[nDim_2pNf*nDim_2pNf]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)
///////////////////////////////////////

	//printf("\n 6"); getchar();

	nNumOfLogPointsf = 0; // nScalef;
	nLenOfSquaref = nDim_2pNf;

	for (iScalef = 0; iScalef < nScalef; iScalef++)
	{
		nLenOfSquaref = nLenOfSquaref / 2;

		if (nLenOfSquaref < 2)
		{
			break;
		}//if (nLenOfSquaref < 2)
		
		nResf = NumOfObjectSquaresAndLogPoint_WithResolution(
			nDim_2pNf, //const int nDim_2pNf,

			nLenOfSquaref, //const int nLenOfSquaref,

			sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

			&sImageEmbeddedf_ForLacunarityBlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

			nNumOfObjectSquaresTotf, //int &nNumOfObjectSquaresTotf,
			fLogPointf); // float &fLogPointf);

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			return UNSUCCESSFUL_RETURN;
		}//if (nResf == UNSUCCESSFUL_RETURN)

		nNumOfLogPointsf += 1;

		fNegLogOfLenOfSquare_Arrf[iScalef] = -log((float)(nLenOfSquaref));
		fLogPoints_Arrf[iScalef] = fLogPointf;

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'SetOfLogPoints_ForFractal_Dim': iScalef = %d, nLenOfSquaref = %d, fLogPoints_Arrf[%d] = %E", iScalef, nLenOfSquaref, iScalef, fLogPoints_Arrf[iScalef]);
		fprintf(fout_lr, "\n\n 'SetOfLogPoints_ForFractal_Dim': iScalef = %d, nLenOfSquaref = %d, fLogPoints_Arrf[%d] = %E", iScalef, nLenOfSquaref, iScalef,fLogPoints_Arrf[iScalef]);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	}//for (iScalef = 0; iScalef < nScalef; iScalef++)

return SUCCESSFUL_RETURN;
}//int SetOfLogPoints_ForFractal_Dim(..

//////////////////////////////////////////////////////////////////////////////////////////
//https://www.varsitytutors.com/hotmath/hotmath_help/topics/line-of-best-fit
void SlopeOfAStraightLine(
	const int nDimf,

	const float fX_Arrf[],
	const float fY_Arrf[],

	float &fSlopef)
{
	int
		i;
	float
		fX_Diff,
		fX_Averf = 0.0,
		fY_Averf = 0.0,

		fSumForNumeratorf = 0.0,
		fSumForDenominatorf = 0.0;

	for (i = 0; i < nDimf; i++)
	{
		fX_Averf += fX_Arrf[i];
		fY_Averf += fY_Arrf[i];

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'SlopeOfAStraightLine' 1: i = %d, fX_Averf = %E, fY_Averf = %E, fY_Arrf[i] = %E", i, fX_Averf, fY_Averf, fY_Arrf[i]);
		fprintf(fout_lr, "\n\n 'SlopeOfAStraightLine'   1: i = %d, fX_Averf = %E, fY_Averf = %E, fY_Arrf[i] = %E", i, fX_Averf, fY_Averf, fY_Arrf[i]);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	}//for (i = 0; i < nDimf; i++)

	fX_Averf = fX_Averf / nDimf;
	fY_Averf = fY_Averf / nDimf;

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'SlopeOfAStraightLine': nDimf = %d, fX_Averf = %E, fY_Averf = %E", nDimf, fX_Averf, fY_Averf);
	fprintf(fout_lr, "\n\n 'SlopeOfAStraightLine': nDimf = %d, fX_Averf = %E, fY_Averf = %E", nDimf, fX_Averf, fY_Averf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
	for (i = 0; i < nDimf; i++)
	{
		fX_Diff = fX_Arrf[i] - fX_Averf;

		fSumForNumeratorf += fX_Diff * (fY_Arrf[i] - fY_Averf);

		fSumForDenominatorf += fX_Diff * fX_Diff;
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'SlopeOfAStraightLine': i = %d, fSumForNumeratorf = %E, fSumForDenominatorf = %E", i, fSumForNumeratorf, fSumForDenominatorf);
		fprintf(fout_lr, "\n\n 'SlopeOfAStraightLine': i = %d, fSumForNumeratorf = %E, fSumForDenominatorf = %E", i, fSumForNumeratorf, fSumForDenominatorf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
	}//for (i = 0; i < nDimf; i++)

	if (fSumForDenominatorf > 0.0)
	{
		fSlopef = fSumForNumeratorf / fSumForDenominatorf;
	}//if (fSumForDenominatorf > 0.0)
	else
		fSlopef = fLarge;

}//void SlopeOfAStraightLine(...
 ///////////////////////////////////////////////////////////////////////////////////////

int MomentOf_Q_order_AtFixedLenOfSquare(

	const int nDim_2pNf,

	const int nLenOfSquaref,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	const float fQf,
	const int nIntOrFloatf, //1 -- int, 0 -- float
///////////////////////////////////////
	int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

	float &fMomentOf_Q_OrderAtFixedLenf) 
{
	int MassesOfAll_ObjectSquares_AtFixedLenOfSquare(
		const int nDim_2pNf,

		const int nLenOfSquaref,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		int &nNumOfSquaresInImageTotf,

		int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

		int &nMassOfImageTotf,
		int nMassesOfSquaresArrf[]); //[nNumOfSquaresInImageTotf]

	float PowerOfAFloatNumber(
		const int nIntOrFloatf, //1 -- int, 0 -- float
		const int nPowerf,
		const float fPowerf,

		const float fFloatInitf); //fFloatInitf != 0.0
	int
		iSquareOccurrenceIntervalf,
		iSquaref,

		nPowerf,
		iWidSquaresf,
		iLenSquaresf,
		nNumOfSquaresInImageSidef = nDim_2pNf / nLenOfSquaref,

		nNumOfSquaresInImagef, // = nNumOfSquaresInImageSidef* nNumOfSquaresInImageSidef,

		nNumOfSquaresInImageTotf,
		//nNumOfNonZero_ObjectSquaresTotf,

		nNumOfNonZero_ObjectSquaresCurf,
		nMassOfASquaref,

		nMassOfImageTotf,
		nMassOfASquareMaxf = nLenOfSquaref * nLenOfSquaref,

		nResf;

	float
		fMassProportionForASquaref,
		fPowerf,
		fMomentOf_Q_OrderAtFixedLenCurf;
	/////////////////////////////

	if (nIntOrFloatf == 1)
	{
		nPowerf = (int)(fQf);
		fPowerf = -fLarge;
	} //if (nIntOrFloatf == 1)
	else if (nIntOrFloatf == 0)
	{
		nPowerf = -nLarge;
		fPowerf = fQf;
	}//else if (nIntOrFloatf == 0)
	else
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquare': nIntOrFloatf = %d", nIntOrFloatf);
		fprintf(fout_lr, "\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquare': nIntOrFloatf = %d", nIntOrFloatf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		return UNSUCCESSFUL_RETURN;
	}//else

	nNumOfSquaresInImagef = nNumOfSquaresInImageSidef * nNumOfSquaresInImageSidef;

	if (nNumOfSquaresInImagef <= 1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquare': nNumOfSquaresInImagef = %d <= 1", nNumOfSquaresInImagef);
		fprintf(fout_lr, "\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquare': nNumOfSquaresInImagef = %d <= 1", nNumOfSquaresInImagef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		return UNSUCCESSFUL_RETURN;
	}//if (nNumOfSquaresInImagef <= 1)
//////////////////////////////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'MomentOf_Q_order_AtFixedLenOfSquare': nLenOfSquaref = %d, nDim_2pNf = %d, fQf = %E, nIntOrFloatf = %d", nLenOfSquaref, nDim_2pNf, fQf,nIntOrFloatf);
	fprintf(fout_lr, "\n\n 'MomentOf_Q_order_AtFixedLenOfSquare': nLenOfSquaref = %d, nDim_2pNf = %d, fQf = %E, nIntOrFloatf = %d", nLenOfSquaref, nDim_2pNf, fQf, nIntOrFloatf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
////////////////////////////////////////////////////////////////
	int *nMassesOfSquaresArrf;
	nMassesOfSquaresArrf = new int[nNumOfSquaresInImagef];

	if (nMassesOfSquaresArrf == nullptr)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in dynamic memory allocation for 'MomentOf_Q_order_AtFixedLenOfSquare'");
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'MomentOf_Q_order_AtFixedLenOfSquare'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nMassesOfSquaresArrf == nullptr)

	nResf = MassesOfAll_ObjectSquares_AtFixedLenOfSquare(
		nDim_2pNf, //const int nDim_2pNf,

		nLenOfSquaref, //const int nLenOfSquaref,

		sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

		sImageEmbeddedf_ForLacunarityBlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		nNumOfSquaresInImageTotf, //int &nNumOfSquaresInImageTotf,

		nNumOfNonZero_ObjectSquaresTotf, //int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

		nMassOfImageTotf, //int &nMassOfImageTotf,

		nMassesOfSquaresArrf); // int nMassesOfSquaresArrf[]); //[nNumOfSquaresTotf]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		delete[] nMassesOfSquaresArrf;
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

	if (nNumOfNonZero_ObjectSquaresTotf <= nNumOfNonZero_ObjectSquaresTotMin) //nNumOfNonZero_ObjectSquaresTotMin == 1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n Too few object squares in 'MomentOf_Q_order_AtFixedLenOfSquare': nNumOfNonZero_ObjectSquaresTotf = %d <= 1",nNumOfNonZero_ObjectSquaresTotf);

		fprintf(fout_lr, "\n\n Too few object squares in 'MomentOf_Q_order_AtFixedLenOfSquare': nNumOfNonZero_ObjectSquaresTotf = %d <= 1",	nNumOfNonZero_ObjectSquaresTotf);
		//printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		fMomentOf_Q_OrderAtFixedLenf = 0.0;

		delete[] nMassesOfSquaresArrf;
		return (-2);
	}//if (nNumOfNonZero_ObjectSquaresTotf <= nNumOfNonZero_ObjectSquaresTotMin) //nNumOfSquareOccurrence_Intervalsf)
//////////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
//	printf("\n\n 'MomentOf_Q_order_AtFixedLenOfSquare': nLenOfSquaref = %d,  nNumOfSquaresInImagef = %d, nNumOfSquaresInImageTotf = %d,  nNumOfNonZero_ObjectSquaresTotf = %d",
	//	nLenOfSquaref, nNumOfSquaresInImagef, nNumOfSquaresInImageTotf, nNumOfNonZero_ObjectSquaresTotf);

	fprintf(fout_lr, "\n\n 'MomentOf_Q_order_AtFixedLenOfSquare': nLenOfSquaref = %d,  nNumOfSquaresInImagef = %d, nNumOfSquaresInImageTotf = %d,  nNumOfNonZero_ObjectSquaresTotf = %d",
		nLenOfSquaref, nNumOfSquaresInImagef, nNumOfSquaresInImageTotf, nNumOfNonZero_ObjectSquaresTotf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	if (nNumOfSquaresInImagef != nNumOfSquaresInImageTotf)
	{

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquare': nNumOfSquaresInImagef = %d != nNumOfSquaresInImageTotf = %d",
			nNumOfSquaresInImagef, nNumOfSquaresInImageTotf);
		fprintf(fout_lr, "\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquare': nNumOfSquaresInImagef = %d != nNumOfSquaresInImageTotf = %d",
			nNumOfSquaresInImagef, nNumOfSquaresInImageTotf);

		printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
	}//if (nNumOfSquaresInImagef != nNumOfSquaresInImageTotf)

	///////////////////////////////////////////////////////////////
	nNumOfNonZero_ObjectSquaresCurf = 0;
	fMomentOf_Q_OrderAtFixedLenf = 0.0;

	for (iSquaref = 0; iSquaref < nNumOfSquaresInImageTotf; iSquaref++)
	{
		nMassOfASquaref = nMassesOfSquaresArrf[iSquaref];

		if (nMassOfASquaref > 0)
		{
			nNumOfNonZero_ObjectSquaresCurf += 1;

			fMassProportionForASquaref = (float)(nMassOfASquaref) / (float)(nMassOfImageTotf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n iSquaref = %d, fMassProportionForASquaref = %E, nMassOfASquaref = %d, nMassOfImageTotf = %d",
				iSquaref, fMassProportionForASquaref, nMassOfASquaref, nMassOfImageTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			if (nMassOfASquaref < 0 || nMassOfASquaref > nMassOfImageTotf)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquare': nMassOfASquaref < 0 || nMassOfASquaref > nMassOfImageTotf");
				fprintf(fout_lr, "\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquare': nMassOfASquaref < 0 || nMassOfASquaref > nMassOfImageTotf");

				printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				delete[] nMassesOfSquaresArrf;
				return UNSUCCESSFUL_RETURN;
			}//if (nMassOfASquaref < 0 || nMassOfASquaref > nMassOfImageTotf)

			fMomentOf_Q_OrderAtFixedLenCurf =  PowerOfAFloatNumber(
								nIntOrFloatf, //const int nIntOrFloatf, //1 -- int, 0 -- float
				nPowerf, //const int nPowerf,
				fPowerf, //const float fPowerf,

				fMassProportionForASquaref); // const float fFloatInitf); //fFloatInitf != 0.0

			fMomentOf_Q_OrderAtFixedLenf += fMomentOf_Q_OrderAtFixedLenCurf;
		} //if (nMassOfASquaref > 0)

	}//for (iSquaref = 0; iSquaref < nNumOfSquaresInImageTotf; iSquaref++)
/////////////////////////////////

	delete[] nMassesOfSquaresArrf;
	return SUCCESSFUL_RETURN;
} // int MomentOf_Q_order_AtFixedLenOfSquare(...
//////////////////////////////////////////////////////////////////////

float PowerOfAFloatNumber(
	const int nIntOrFloatf, //1 -- int, 0 -- float
	const int nPowerf,
	const float fPowerf,

	const float fFloatInitf) //fFloatInitf != 0.0
{
	int
		iPowf;

	float
		fPowerCurf = fFloatInitf;
	
	if (nIntOrFloatf == 1)
	{
		if (nPowerf == 0)
			return 1.0;
		else if (nPowerf > 0)
		{
			for (iPowf = 1; iPowf < nPowerf; iPowf++)
			{
				fPowerCurf = fPowerCurf * fFloatInitf;
			}//for (iPowf = 1; iPowf < nPowerf; iPowf++)

		}//else if (nPowerf > 0)
		else if (nPowerf < 0)
		{
			for (iPowf = 1; iPowf < nPowerf; iPowf++)
			{
				fPowerCurf = fPowerCurf * fFloatInitf;
			}//for (iPowf = 1; iPowf < nPowerf; iPowf++)

			fPowerCurf = 1.0 / fPowerCurf;
		} //else if (nPowerf < 0)

	}//if (nIntOrFloatf == 1)
	else if (nIntOrFloatf == 0)
	{
		fPowerCurf = powf(fFloatInitf, fPowerf);

	}//else if (nIntOrFloatf == 0)

	return fPowerCurf;
}//float PowerOfAFloatNumber(

  //printf("\n\n Please press any key:"); getchar();



