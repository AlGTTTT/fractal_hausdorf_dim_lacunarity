
#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

#include "Fractal_Hausdorf_Dim_function.cpp"

int main()
{
	int doFractal_DimensionOf_ColorImage(

		const Image& image_in,

		//const PARAMETERS_REMOVAL_OF_LABELS *sPARAMETERS_REMOVAL_OF_LABELSf,
		float fOneDim_Lacunar_Arrf[], //[nNumOfFeasForLacunar_AtIntensity_Range] = [nNumOfLenOfSquareMax*3 + nNumOfLenOfSquareMax*nNumOfSquareOccurrence_Intervals]
		float &fFractal_Dimension);

	int
		nRes;

	float
		fOneDim_Lacunar_Arr[nNumOfLacunarFeasFor_OneDimTot], //[nNumOfFeasForLacunar_AtIntensity_Range] = [nNumOfLenOfSquareMax*3 + nNumOfLenOfSquareMax*nNumOfSquareOccurrence_Intervals]
		fFractal_Dimension_Out;

//	float
	//	fOneDim_Lacunar_Arr[nNumOfLacunarFeasFor_OneDimTot]; //[nNumOfLacunarFeasFor_OneDimTot]
		////////////////////////////////////////////////////////////////////////////////////////////////
/*
	fout_lr = fopen("wMain_RemovingLabels.txt", "w");

	if (fout_lr == NULL)
	{
		printf("\n\n fout_lr == NULL");
		getchar(); exit(1);
	} //if (fout_lr == NULL)
*/
	Image image_in;

	image_in.read("Row A Day 1.png");
	//image_in.read("Row A Day 3.png");

	//image_in.read("Row B Day 1.png");
	//image_in.read("Row B Day 3.png");

	//image_in.read("Row C Day 1.png");
	//image_in.read("Row C Day 3.png");

	Image image_out;
	
	nRes = doFractal_DimensionOf_ColorImage(
				image_in, // Image& image_in,

		fOneDim_Lacunar_Arr, //[nNumOfLacunarFeasFor_OneDimTot], //[nNumOfFeasForLacunar_AtIntensity_Range] = [nNumOfLenOfSquareMax*3 + nNumOfLenOfSquareMax*nNumOfSquareOccurrence_Intervals]
		fFractal_Dimension_Out); // 

	if (nRes == UNSUCCESSFUL_RETURN) // -1
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n The image has not been processed properly.");

		fprintf(fout_lr,"\n\n The image has not been processed properly.");

		fflush(fout_lr); //debugging;
		printf("\n\n The image has not been processed properly: please press any key to exit");	getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN; //
	} // if (nRes == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n After 'doFractal_DimensionOf_ColorImage': fFractal_Dimension_Out = %E", fFractal_Dimension_Out);
	fprintf(fout_lr, "\n\n After 'doFractal_DimensionOf_ColorImage': fFractal_Dimension_Out = %E", fFractal_Dimension_Out);

	fflush(fout_lr); //debugging;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	//image_out.write("01-144_LCC_mask_NoLabels.png");

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n Please press any key to exit"); getchar(); //exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	return SUCCESSFUL_RETURN;
} //int main()

//printf("\n\n Please press any key:"); getchar();